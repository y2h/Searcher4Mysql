#(Db ?? "db is not exist")
#set(sql="select * from area")
#(Db.query(sql) ?? "no result")

#for(x : Db.query(sql) )
  #(x[0].name)
  #(x[0].lv)
#end


#for(x:jsonUtil.toJsonStr(Db.query(sql)))
#(jsonUtil.toJsonStr(Db.query(sql)[0]))
#(jsonUtil.toJsonStr(Db.query(sql)[1]))
#(for.size)
#end

#demo()
 这里是 demo body 的内容
#end

今天的日期是： #now()

#number(3.1415926, "#.##")

#number(0.9518, "#.##%")

#number(300000, "光速为每秒，### 公里。")

#(1+2/3*4)
#(1>2)
#("abcdef".substring(0,3))
#set(arr=[1,2,3])
#(arr[1])
#set(menu='index')
#(menu=='index' ? 'current' : 'normal')
#(menu!="index" ? "current" : "normal")
#if(menu=='index')
    #(menu)
#end
#if(menu!='index')
    #(normal)
#else
    #(menu)
#end

#set(map={k1:123,"k2":"abc", "k3":gmodel})
#(map.k1)
#(map.k2)
#(map["k1"])
#(map["k2"])
#(map.get("k1"))
#({1:'自买', 2:'跟买'}.get(1))
#({1:'自买', 2:'跟买'}[2])

### 与双问号符联合使用支持默认值
#({1:'自买', 2:'跟买'}.get(999) ?? '其它')

// 定义数组 array，并为元素赋默认值
#set(array = [123, "abc", true])
#set(array2 = [2123, "2abc", false])

// 获取下标为 1 的值，输出为: "abc"
#(array[1])

// 将下标为 1 的元素赋值为 false，并输出
#(array[1] = false, array[1])
###set(array = [ 123, "abc", true, a && b || c, 1 + 2, obj.doIt(x) ])

#for(x : [1..10])
   #(x)
#end

// 对 Map 进行迭代
#for(x : map)
  #(x.key)
  #(x.value)
#end

// 对 List、数组、Set 这类结构进行迭代
#for(x : array)
   #(for.size)
   #(for.index)
   #(for.count)
   #(for.first)
   #(for.last)
   #(for.odd)
   #(for.even)

  #for(x : array2)
     #(for.outer)
     #(for.outer.index)
     #(for.index)
  #end
#end

### 判断每月天数
#for(month:[1..12])
    #switch (month)
      #case (1, 3, 5, 7, 8, 10, 12)
        #(month) 月有 31 天
      #case (2)
        #(month) 月平年有28天，闰年有29天
      #default
       #(month) 月平年有30天
    #end
#end

#set(x = 123)
#set(a = 1, b = 2, c = a + b)
#set(array[0] = 123)
#set(map["key"] = 456)
 
#(x)  #(c)  #(array[0])  #(map.key)  #(map["key"])

#(x = 123, y = "abc", array = [1, "a", true], map = {k1:v1})

#(gmodel.baseResourcesPath ?? "gmodel is not exist")
