### https://www.jfinal.com/share/1655
### 使用sql模板进行条件拼装
#sql("templateSql")
    #(querySql)  ###查询使用的sql
    #for(x : cond)  ###处理条件cond参数
        #(for.first ? "where": "and")
        #if(x.key.contains(" like"))  ###拼装like条件
            #(x.key) concat('%', #para(x.value), '%')
        #elseif(x.key.contains(" in"))  ###拼装in条件
            #(x.key) #("(")
            #for(inVal : x.value.split(","))
                #(for.index == 0 ? "" : ",")#para(inVal)
            #end
            #(")")
        #else  ###拼装其余条件，= > < >= <= != and
            #(x.key) #para(x.value)
        #end
    #end
    #(orderSql??)
#end

#sql("getUsers")
	select * from user
#end

#sql("get1User")
	select * from user where id=10
#end

#sql("getColumns")
	select #(columns ?? "*") from user
#end

#sql("lists")
	select #(columns ?? "*") from #(table)
#end

#sql("list")
	select #(columns ?? "*") from #(table) #@where()
#end