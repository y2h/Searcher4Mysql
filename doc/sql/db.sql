CREATE DATABASE `test1` /*!40100 COLLATE 'utf8mb4_general_ci' */;

DROP TABLE  IF EXISTS `user`;
CREATE TABLE `user` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT '0',
	`age` INT NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
)
COLLATE='utf8mb4_general_ci';