package com.cyy.learn.JavaFX3D

import javafx.scene.shape.Sphere
import tornadofx.*

class Sphere3D : View("Sphere3D"){
    override val root=group {
        Sphere()
    }

}