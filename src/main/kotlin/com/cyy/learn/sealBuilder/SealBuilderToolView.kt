package com.cyy.learn.sealBuilder

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.ImageView
import javafx.scene.paint.Color
import tornadofx.*

val contentTextField = SimpleStringProperty("")
val fontFamilyChoiceBox = SimpleStringProperty()
val fontFamilyChoiceBoxs = listOf("")
val contentTextField1 = SimpleStringProperty("")
val fontFamilyChoiceBox1 = SimpleStringProperty()
val fontFamilyChoiceBox1s = listOf("")
val contentTextField2 = SimpleStringProperty("")
val fontFamilyChoiceBox2 = SimpleStringProperty()
val fontFamilyChoiceBox2s = listOf("")
val contentTextField3 = SimpleStringProperty("")
val fontFamilyChoiceBox3 = SimpleStringProperty()
val fontFamilyChoiceBox3s = listOf("")
val onColorColorPicker = SimpleObjectProperty(Color.BLACK)
lateinit var codeImageView: ImageView

class SealBuilderToolView : View("SealBuilderTool") {
    val C: SealBuilderToolController by inject()

    override val root = borderpane {
        top = vbox {
            hbox {
                label("主 文 字 ")
                textfield(contentTextField) { promptText = "输入正文文字" }
                label("字体")
                choicebox(fontFamilyChoiceBox,fontFamilyChoiceBoxs)
                label("字体大小")
                spinner(1,10,5){
                    prefWidth=60.0
                }
                label("字距：")
                label("边距：")
                spinner(1,10,5){
                    prefWidth=60.0
                    isEditable=true
                }
            }
            hbox {
                label("副 文 字 ")
                textfield(contentTextField1) { promptText = "null" }
                label("字体")
                choicebox(fontFamilyChoiceBox1,fontFamilyChoiceBox1s)
                label("字体大小")
                label("字距：")
                label("边距：")
            }
            hbox {
                label("抬头文字")
                textfield(contentTextField2) { promptText = "null" }
                label("字体")
                choicebox(fontFamilyChoiceBox2,fontFamilyChoiceBox2s)
                label("字体大小")
                label("字距：")
                label("边距：")
            }
            hbox {
                label("中心文字")
                textfield(contentTextField3) { promptText = "null" }
                label("字体")
                choicebox(fontFamilyChoiceBox3,fontFamilyChoiceBox3s)
                label("字体大小")
                label("字距：")
                label("边距：")
            }
            hbox {
                label("边 线 圆 ")
                label("线宽度")
                label("宽半径")
                label("高半径")
            }
            hbox {
                label("内边线圆")
                label("线宽度")
                label("宽半径")
                label("高半径")
            }
            hbox {
                label("内 线 圆 ")
                label("线宽度")
                label("宽半径")
                label("高半径")
            }
            hbox {
                label("印章颜色：")
                colorpicker(onColorColorPicker)
                label("图片输出尺寸")
                button("保存") {
                    action {
                        C.saveAction()
                    }
                }
            }
            center {
            }
            hbox {
                imageview {
                    fitHeight = 320.0
                    fitHeight = 320.0
                    isPickOnBounds = true
                    isPreserveRatio = true
                }
            }
        }
    }
}