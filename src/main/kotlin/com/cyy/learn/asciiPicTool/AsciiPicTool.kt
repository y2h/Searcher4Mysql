package com.cyy.learn.asciiPicTool

import com.cyy.util.FileChooserUtil
import com.cyy.util.ImageUtil
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.control.TextField
import javafx.scene.layout.Priority
import tornadofx.*
import com.cyy.util.TooltipUtil
import com.jfoenix.controls.JFXComboBox
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.image.ImageView

val imageSize: ObservableList<String> = FXCollections.observableArrayList<String>("不压缩", "60*60", "120*120", "256*256", "512*512")

val codeTextArea = SimpleStringProperty("")
val filePath = SimpleStringProperty("")
lateinit var filePathTextField: TextField
lateinit var imageImageView: ImageView
lateinit var imageSizeComboBox: JFXComboBox<String>

class AsciiPicTool : View("AsciiPicTool") {
    val C: AsciiPicToolController by inject()
    override val root = anchorpane {
        borderpane {
            paddingAll = 10.0

            top = hbox(5) {
                alignment = Pos.CENTER
                paddingBottom = 10.0
                label("图片地址：")
                filePathTextField=textfield(filePath){
                    textProperty().addListener { observable: ObservableValue<out String>, oldValue: String, newValue: String ->
                        try {
                            val image = ImageUtil.getFXImage(newValue)
                            //                try {
                            //                    image = SwingFXUtils.toFXImage(Imaging.getBufferedImage(new File(newValue)), null);
                            //                } catch (Exception e) {
                            //                    log.error(e.getMessage());
                            //                    image = new Image("file:" + newValue);
                            //                }
                            imageImageView.image = image
                            if (image != null) {
                                imageImageView.fitWidth = image.width
                            }
                            if (image != null) {
                                imageImageView.fitHeight = image.height
                            }
                        } catch (e: Exception) {
//                            log.error("图片加载失败：", e)
                            TooltipUtil.showToast("图片加载失败：" + e.message)
                        }
                    }
                }

                button("选择") {
                    action {
                        C.filePathAction()
                    }
                }
                button("生成banner") {
                    action {
                        C.buildBannerAction()
                    }
                }
                button("转Base64码") {
                    action {
                        C.buildBase64Action()
                    }
                }

                imageSizeComboBox=JFXComboBox(imageSize)
                imageSizeComboBox.selectionModel.select(0)
                add(imageSizeComboBox)

            }
            center = hbox(5) {
                alignment = Pos.CENTER
                vbox(5) {
                    scrollpane {
                        imageImageView=imageview() {
                            fitHeight = 150.0
                            fitWidth = 150.0
                            isPreserveRatio = true
                            isPickOnBounds = true
                        }
                        maxWidth = 400.0
                        minHeight = 150.0

                    }
                    button("保存图片") {
                        action {
                            C.saveImageAction()
                        }
                    }
                    button("转换为Excel图") {
                        action {
                            C.saveImageToExcelAction()
                        }
                    }
                }
                textarea(codeTextArea) {
                    promptText = "生成的code"
                    hgrow = Priority.ALWAYS
                    textProperty().addListener { observable: ObservableValue<out String>, oldValue: String, newValue: String -> C.buildBase64ToImage(newValue) }
                }
            }
        }
    }
    init {
        FileChooserUtil.setOnDrag(filePathTextField, FileChooserUtil.FileType.FILE)
    }
}