package com.cyy.learn.shortURL

import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.*
import tornadofx.*
import java.util.HashMap

val longURLTextField = SimpleStringProperty("")
val shortURLTextField = SimpleStringProperty("")
val aliasUrlTextField = SimpleStringProperty("")
val resultTextArea = SimpleStringProperty("")
val choice = SimpleStringProperty("")
lateinit var shortURLServiceChoiceBox: ChoiceBox<String>
lateinit var revertButton: Button
lateinit var convertButton: Button
lateinit var aliasUrlCheckBox: CheckBox
val shortURLService = listOf("百度", "新浪", "缩我")
val shortSiteMap = HashMap<String, String>()
val shortSiteToggleGroup=ToggleGroup()
val rbList= mutableListOf<RadioButton>()

class ShortURLView : View("ShortURL") {
    val C: ShortURLController by inject()

    override val root = vbox(10) {
        paddingAll = 10.0
        hbox(5) {
            label("长网址：")
            textfield(longURLTextField) {
                text = "http://www.baidu.com"
                prefWidth=400.0
            }
            button("复制") {
                action {
                    C.longURLCopyAction()
                }
            }
            convertButton=button("转换") {
                action {
                    C.convertAction()
                }
            }
        }

        hbox(5) {
            label("短网址：")
            textfield(shortURLTextField){
                prefWidth=340.0
            }
            shortURLServiceChoiceBox = choicebox(choice, shortURLService) {
                selectionModel.select(0)
            }

            button("复制") {
                action {
                    C.shortURLCopyAction()
                }
            }
            revertButton=button("还原") {
                action {
                    C.revertAction()
                }
            }
        }
        hbox(5) {
            aliasUrlCheckBox=checkbox("自定义后缀：") { isSelected = true }
            textfield(aliasUrlTextField) { promptText = "输入字母, 数字, 破折号" }

        }
        textarea(resultTextArea) {
            text = """
                说明：
                1、百度短网址API：http://www.baidu.com/search/dwz.html
                2、新浪短网址API：新浪短网址http://t.cn/需要授权才可使用，此处转换中不列出，详细API说明如下：
                                http://open.weibo.com/wiki/2/short_url/shorten
                                http://open.weibo.com/wiki/2/short_url/expand
                3、缩我短网址：http://www.suo.im
            """.trimIndent()
        }
    }

    init {
        shortSiteMap["sina.lt"] = "sinalt"
        shortSiteMap["t.cn"] = "sina"
        shortSiteMap["dwz.cn"] = "dwz"
        shortSiteMap["qq.cn.hn"] = "qq.cn.hn"
        shortSiteMap["tb.cn.hn"] = "tb.cn.hn"
        shortSiteMap["jd.cn.hn"] = "jd.cn.hn"
        shortSiteMap["tinyurl.com"] = "tinyurl"
        shortSiteMap["goo.gl"] = "googl"
        shortSiteMap["j.mp"] = "jmp"
        shortSiteMap["bit.ly"] = "bitly"
        for ((i, entry) in shortSiteMap.entries.withIndex()) {
            val shortSiteRadioButtons = RadioButton(entry.key)
            shortSiteRadioButtons.userData = entry.value
            shortSiteRadioButtons.layoutX = 13.0 + 80 * i
            shortSiteRadioButtons.layoutY = 114.0
            shortSiteRadioButtons.toggleGroup = shortSiteToggleGroup
            shortSiteRadioButtons.isVisible=false
            rbList.add(shortSiteRadioButtons)
        }
        shortSiteToggleGroup.selectToggle(rbList[0])

        root.children.addAll(rbList)
        initEvent()
    }

    private fun initEvent() {
        shortURLServiceChoiceBox.valueProperty().addListener { observable, oldValue, newValue ->
//            root.children.removeAll<Any?>(shortSiteToggleGroup.toggles)
            rbList.forEach{
                it.isVisible=false
            }
            revertButton.isDisable = false
            aliasUrlCheckBox.isDisable = true
            when (newValue) {
                shortURLService[0] -> aliasUrlCheckBox.isDisable = false  //百度http://dwz.cn

                shortURLService[1] -> {//新浪http://sina.lt
//                    root.children.addAll(rbList)
                    rbList.forEach{
                        it.isVisible=true
                    }
                }
                shortURLService[2] ->revertButton.isDisable = true //缩我http://www.suo.im
            }
        }
    }
}