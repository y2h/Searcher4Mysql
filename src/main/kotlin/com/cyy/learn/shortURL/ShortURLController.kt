package com.cyy.learn.shortURL

import org.apache.commons.lang3.StringUtils
import tornadofx.*
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import javafx.application.Platform


class ShortURLController : Controller() {
    fun longURLCopyAction(){
        val selection = StringSelection(longURLTextField.value)// 获取系统剪切板，复制长网址
        Toolkit.getDefaultToolkit().systemClipboard.setContents(selection, selection)
    }
    fun convertAction(){
        val shortURLServiceType = shortURLServiceChoiceBox.value
        val longUrlText = longURLTextField.value.trim()
        if (StringUtils.isEmpty(longUrlText)) {
            return
        }
        Platform.runLater { convertButton.isDisable = true }
        shortURLTextField.value=""
        when (shortURLServiceType) {
            shortURLService[0] -> {
                var shortURL: String? = null
                if (aliasUrlCheckBox.isSelected) {
                    shortURL = ShortURLService.baiduToShort(longUrlText, aliasUrlTextField.value)
                } else {
                    shortURL = ShortURLService.baiduToShort(longUrlText, null!!)
                }
                shortURLTextField.value=shortURL
            }
            shortURLService[1] -> {
                var shortURL: String? = null
                val site = shortSiteToggleGroup.selectedToggle.userData.toString()
                shortURL = ShortURLService.sinaToShort(longUrlText, site)
                shortURLTextField.value=shortURL
            }
            shortURLService[2] -> {
                val shortURL = ShortURLService.suoImToShort(longUrlText)
                shortURLTextField.value=shortURL
            }
        }
        Platform.runLater { convertButton.setDisable(false) }
    }
    fun shortURLCopyAction(){
    }
    fun revertAction(){
        val shortUrlText=shortURLTextField.value.trim()
        if (StringUtils.isEmpty(shortUrlText)) {
            return
        }
        longURLTextField.value=""
        if (shortURLService[0] == choice.value) {
            val shortURL = ShortURLService.baiduToLongURL(shortUrlText)
            longURLTextField.value=shortURL
        } else if (shortURLService[1] == choice.value) {
            val shortURL = ShortURLService.sinaToLongURL(shortUrlText)
            longURLTextField.value=shortURL
        }
    }
}