package com.cyy.learn.shortURL

import cn.hutool.log.StaticLog.info
import com.alibaba.fastjson.JSON
import com.cyy.util.HttpClientUtil
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang3.StringUtils


object ShortURLService {
    /**
     * 百度转换为短网址.
     */
    fun baiduToShort(longURL: String, alias: String): String {
        var shortURL = "转换错误"
        try {
            val params = "{\"url\":\"$longURL\"}"
            val url = "https://dwz.cn/admin/v2/create"
            val Token = "517cfcf7869ce43dec7b8e076f870652"
            val client = OkHttpClient()
            val body = params.toRequestBody("application/json; charset=UTF-8".toMediaTypeOrNull())
            val request = Request.Builder().url(url).post(body).addHeader("Token", Token).build()
            val response = client.newCall(request).execute()
            val data = response.body?.string()
            info("返回值：$data")
            val jsonObject = JSON.parseObject(data)
            if (jsonObject.getIntValue("Code") === 0) {
                shortURL = jsonObject.getString("ShortUrl")
            } else {
                shortURL = jsonObject.getString("ErrMsg")
            }
        } catch (e: Exception) {
            Error("转换出错：", e)
        }

        return shortURL
    }

    /**
     * 百度还原为长网址.
     */
    fun baiduToLongURL(shortURL: String): String {
        var longURL = "转换错误"
        try {
            val params = "{\"shortUrl\":\"$shortURL\"}"
            val url = "https://dwz.cn/admin/v2/query"
            val Token = "517cfcf7869ce43dec7b8e076f870652"
            val client = OkHttpClient()
            val body = params.toRequestBody("application/json; charset=UTF-8".toMediaTypeOrNull())
            val request = Request.Builder().url(url).post(body).addHeader("Token", Token).build()
            val response = client.newCall(request).execute()
            val data = response.body?.string()
            info("返回值：$data")
            val jsonObject = JSON.parseObject(data)
            if (jsonObject.getIntValue("Code") === 0) {
                longURL = jsonObject.getString("LongUrl")
            } else {
                longURL = jsonObject.getString("ErrMsg")
            }
        } catch (e: Exception) {
            Error("转换出错：", e)
        }

        return longURL
    }

    /**
     * 新浪转换为短网址.
     */
    fun sinaToShort(longURL: String, site: String): String {
        var shortURL = "转换错误"
        try {
            val url = StringBuffer("http://dwz.wailian.work/api.php?")
            url.append("url=").append(Base64.encodeBase64String(longURL.toByteArray()))
            url.append("&site=").append(site)
            println(url.toString())
            val refererUrl = "http://dwz.wailian.work/"
            val data = HttpClientUtil.getHttpDataAsUTF_8(url.toString(), refererUrl)
            val jsonObject = JSON.parseObject(data)
            if ("ok" == jsonObject.getString("result")) {
                shortURL = jsonObject.getJSONObject("data").getString("short_url")
            } else {
                shortURL = jsonObject.getString("data")
            }
            println(data)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return shortURL
    }

    /**
     * 新浪短网址还原.
     */
    fun sinaToLongURL(longURL: String): String {
        var shortURL = "转换错误"
        try {
            val url = StringBuffer("http://dwz.wailian.work/api.php?")
            url.append("url=").append(Base64.encodeBase64String(longURL.toByteArray()))
            url.append("&action=restore")
            println(url.toString())
            val refererUrl = "http://dwz.wailian.work/restore.php"
            val data = HttpClientUtil.getHttpDataAsUTF_8(url.toString(), refererUrl)
            val jsonObject = JSON.parseObject(data)
            if ("ok" == jsonObject.getString("result")) {
                shortURL = jsonObject.getJSONObject("data").getString("short_url")
            } else {
                shortURL = jsonObject.getString("data")
            }
            println(data)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return shortURL
    }

    /**
     * 缩我转换为短网址http://www.suo.im
     */
    fun suoImToShort(longURL: String): String {
        var shortURL = "转换错误"
        try {
            val url = "http://suo.im/api.php?format=json&url=$longURL"
            val refererUrl = "http://www.suo.im/"
            val data = HttpClientUtil.getHttpDataAsUTF_8(url, refererUrl)
            val jsonObject = JSON.parseObject(data)
            if (StringUtils.isEmpty(jsonObject.getString("err"))) {
                shortURL = jsonObject.getString("url")
            } else {
                shortURL = jsonObject.getString("err")
            }
            println(data)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return shortURL
    }
}