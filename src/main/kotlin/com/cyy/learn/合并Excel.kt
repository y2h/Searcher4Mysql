package com.cyy.learn

import cn.hutool.poi.excel.ExcelUtil
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*
import java.io.File

class 合并Excel : View("MergeExcel") {

    val d = stringProperty("""E:\data\四个一百\上报数据\问题台账汇总表""")
    val mergedExcel = stringProperty("""mergedExcel.xlsx""")
    val startRow= intProperty()
    override val root = vbox(5) {
        label("Usage : 将要合并的Excel文件放入同一个文件夹，输入文件夹路径和输出文件名称，点击start merge按钮，开始合并"){
            isWrapText=true
            style {
                fontSize = 16.px
                fontWeight = FontWeight.BOLD
                textFill = Color.RED
            }
        }
        hbox(5) {
            button("open directory") {
                action {
                    //                ExcelUtil.getWriter("book1.xlsx").write(ExcelUtil.getReader("""E:\data\四个一百\上报数据\问题台账汇总表\1.xlsx""").read(5)).close()
                    chooseDirectory {
                        initialDirectory = File(d.value)
                    }?.let {
                        d.value = it.absolutePath
//                        it.absoluteFile?.listFiles()?.forEach {
//                            val w = ExcelUtil.getWriter("book1.xlsx")
//                            w.currentRow = w.rowCount + 1
//                            w.write(ExcelUtil.getReader(it).read(5)).close()
//                        }
                    }
//                val efset = arrayOf(FileChooser.ExtensionFilter("选择配置文件", "*.*"))
//                chooseFile("选择配置文件", efset, FileChooserMode.Single) {
//                    // p初始目录为当前项目目录
//                    initialDirectory = File(File("").canonicalPath)
//                }?.let {
//                    println(ExcelUtil.getReader(it.first()).read(5))
//                    KExcel.open(it.first().absolutePath).use { workbook ->
//                        val sheet = workbook[0]
//
//                        println("""B7=${sheet["B7"]}""")
//
//                        println("B8=${sheet[1, 8]}")
//                        println("B9=${sheet[1, 9]}")
//
//                        sheet["A1"] = "haha"
//                        sheet[3, 7] = 123
//
//                        KExcel.write(workbook, "book2.xlsx")
//                    }
//                }

                }
            }
            textfield(d) {
                useMaxWidth = true
                prefWidth = 700.0
            }
        }
        hbox(5) {
            button("start merge") {
                action {
                    mergeExcel(d.value,mergedExcel.value,startRow.value)
                }
            }
            label("输出文件名称：")
            textfield(mergedExcel) {
                useMaxWidth = true
                prefWidth = 700.0
            }
        }
    }
    fun mergeExcel(dir:String,out:String,startRow:Int){
        File(dir).listFiles()?.forEach {
            val w = ExcelUtil.getWriter(out)
//            w.currentRow = w.rowCount + 1
            w.currentRow = w.rowCount
            w.write(ExcelUtil.getReader(it).read(5)).close()
        }
    }

}
