package com.cyy.learn.characterConverter

import com.cyy.util.RadixUtils
import javafx.scene.control.TextField
import org.apache.commons.codec.binary.Hex
import tornadofx.*
import java.nio.charset.Charset

class CharacterConverterController : Controller() {
    private val characterConverterService = CharacterConverterService()
    private val customCharsetField: TextField? = null

    init {
        buttons.forEachIndexed { index, button ->
            button?.action {
                getButtonActionListener(index + 1)
            }
        }
    }

    fun clearTextFields() {
        encodeTextField.value = ""
        fields.forEach {
            it?.text = ""
        }
    }

    /**
     * 按钮点击事件.
     */
    fun getButtonActionListener(accessibleText: Int) {
        val curCodeType = codeType.value
        val curPrefix = prefix.value
        val curLowUpCase = lowUpCase.value
        // 手动输入的字符编码
        charsets[charsets.size - 1] = customCharsetField?.text?.trim().toString()
        val sort = accessibleText
        if (sort == 0) {
            val input = encodeTextField.value
            clearTextFields()
//                encodeTextField.setText(input)
            if (input.isEmpty()) {
                return
            }
            for (i in 0 until fields.size) {
                try {
                    when (curCodeType) {
                        codeTypes[3] -> fields[i]?.text = characterConverterService.encode16RadixAddPrefix(input, charsets[i], curPrefix)
                        codeTypes[2] -> fields[i]?.text = characterConverterService.encode10RadixAddPrefix(input, charsets[i], curPrefix)
                        codeTypes[1] -> fields[i]?.text = characterConverterService.encode8RadixAddPrefix(input, charsets[i], curPrefix)
                        codeTypes[0] -> fields[i]?.text = characterConverterService.encode2RadixAddPrefix(input, charsets[i], curPrefix)
                    }
                } catch (e: Exception) {
//                    							showExceptionMessage(e)
                    return
                }

            }
        } else {
            var input = fields[sort - 1]?.text
            clearTextFields()
            fields[sort - 1]?.text = input
            input = input?.replace(curPrefix, "") // 去除前缀符
            if (input?.length == 0) {
                return
            }
            try {
                var decodeString = ""
                when (curCodeType) {
                    codeTypes[3] -> decodeString = String(Hex.decodeHex(input), Charset.forName(charsets[sort - 1]))
                    codeTypes[2] -> decodeString = String(Hex.decodeHex(RadixUtils.convertRadixString10To16(input.toString())), Charset.forName(charsets[sort - 1]))
                    codeTypes[1] -> decodeString = String(Hex.decodeHex(RadixUtils.convertRadixString8To16(input.toString())), Charset.forName(charsets[sort - 1]))
                    codeTypes[0] -> decodeString = String(Hex.decodeHex(RadixUtils.convertRadixString2To16(input.toString())), Charset.forName(charsets[sort - 1]))
                }
                encodeTextField.value = decodeString
                if (codeTypes[4] == curCodeType) {
                    encodeTextField.value = "\"" + codeTypes[4] + "\"与Encode String无关！"
                }
                for (i in 0 until fields.size) {
                    if (i != sort - 1) {
                        when (curCodeType) {
                            codeTypes[3] -> fields[i]?.text = characterConverterService.encode16RadixAddPrefix(decodeString, charsets[i], curPrefix)
                            codeTypes[2] -> fields[i]?.text = characterConverterService.encode10RadixAddPrefix(decodeString, charsets[i], curPrefix)
                            codeTypes[1] -> fields[i]?.text = characterConverterService.encode8RadixAddPrefix(decodeString, charsets[i], curPrefix)
                            codeTypes[0] -> fields[i]?.text = characterConverterService.encode2RadixAddPrefix(decodeString, charsets[i], curPrefix)
                            codeTypes[4] -> fields[i]?.text = String(charsets[sort - 1].toByteArray(), Charset.forName(charsets[i]))
                        }
                    }
                }
            } catch (e: Exception) {
                //						showExceptionMessage(e);
                return
            }

        }
        // 大小写
        if (codeTypes[3] == curCodeType || codeTypes[0] == curCodeType) {
            when (curLowUpCase) {
                lowUpCases[0] -> fields.forEach {
                    it?.text = it?.text?.toLowerCase()
                }
                lowUpCases[1] -> fields.forEach {
                    it?.text = it?.text?.toUpperCase()
                }
            }
        }
    }
}
