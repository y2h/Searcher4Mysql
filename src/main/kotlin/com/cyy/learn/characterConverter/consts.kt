package com.cyy.learn.characterConverter

import com.cyy.util.GuiUtils
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Button
import javafx.scene.control.TextField

val encodeTextField = SimpleStringProperty("")
/**
 * 编解码类别.
 */
val codeType = SimpleStringProperty("")
val codeTypes= listOf("二进制", "八进制", "十进制", "十六进制", "乱码解码")
/**
 * 进制前缀符.
 */
val prefix = SimpleStringProperty("")
val prefixs= listOf("空格", "空", "-", "%", "\\u")
/**
 * 进制编码大小写.
 */
val lowUpCase = SimpleStringProperty("")
val lowUpCases= listOf("小写", "大写")

/**
 * 字符集.
 */
val charset = SimpleStringProperty("")
val charsets= mutableListOf(GuiUtils.CHARSET_UTF_16BE, GuiUtils.CHARSET_UTF_16LE,
        GuiUtils.CHARSET_UTF_8, GuiUtils.CHARSET_UTF_16, GuiUtils.CHARSET_GB2312, GuiUtils.CHARSET_GBK,
        GuiUtils.CHARSET_GB18030, GuiUtils.CHARSET_Big5, GuiUtils.CHARSET_ISO_8859_1, "")

//val charsets= mutableListOf(Charsets.UTF_16BE, Charsets.UTF_16LE,
//        Charsets.UTF_8, Charsets.UTF_16, Charsets.ISO_8859_1, GuiUtils.CHARSET_GBK,
//        GuiUtils.CHARSET_GB18030, GuiUtils.CHARSET_Big5, Charsets.ISO_8859_1, "")

val fields = mutableListOf<TextField>()
val buttons = mutableListOf<Button>()


