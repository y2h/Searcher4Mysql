package com.cyy.learn.characterConverter

import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import tornadofx.*

class CharacterConverterView : View("字符转换") {
    val C: CharacterConverterController by inject()

    override val root = borderpane {
        paddingAll=10.0
        center = vbox(5) {
            hbox(5) {
                label("Encode String:")
                textfield(encodeTextField) { promptText = "null" }
                button("Encode") {
                    accessibleText="0"
                    action {
                        C.getButtonActionListener(0)
                    }
                }
            }
            hbox(5) {
                label("编码类别：")
                choicebox(codeType,codeTypes){selectionModel.select(0)}
                label("进制前缀符:")
                choicebox(prefix,prefixs){selectionModel.select(0)}
                label("进制大小写:")
                choicebox(lowUpCase,lowUpCases){selectionModel.select(0)}
                button("清除输入输出") {
                    action {
                        C.clearTextFields()
                    }
                }
            }
        }
    }
    init {
        initView()
    }

    private fun initView(){
        for (i in 0 until charsets.size) {
            val hBox = HBox()
            hBox.alignment = Pos.CENTER
            hBox.spacing = 10.0
            fields.add(TextField())
            if ("" == charsets[i]) {
                hBox.children.add(textfield("UTF-8"){prefWidth=90.0})
            } else {
                hBox.children.add(label(charsets[i]){prefWidth=90.0})
            }
            //			mainAnchorPane.getChildren().add(buttons[i]);
            hBox.children.add(fields[i])
            hBox.children.add(button("Decode"){
                accessibleText=(i + 1).toString()
                action {
                    C.getButtonActionListener(i + 1)
                }
            })
            HBox.setHgrow(fields[i], Priority.ALWAYS)
            root.center.add(hBox)
        }
    }
}


