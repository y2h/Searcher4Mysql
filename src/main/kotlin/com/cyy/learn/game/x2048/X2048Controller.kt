package com.cyy.learn.game.x2048

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.input.*
import tornadofx.*

class X2048Controller : Controller() {
    fun onReset(){
        X2048Service.implInit(X2048Service.size)
    }
    fun onSwipeDown():EventHandler<SwipeEvent>{
        return EventHandler {
//            X2048Service.processCode(KeyCode.DOWN)
            X2048Service.processCode(KeyCode.S)
        }

    }
    fun onSwipeLeft():EventHandler<SwipeEvent>{
        return EventHandler{
//            X2048Service.processCode(KeyCode.LEFT)
            X2048Service.processCode(KeyCode.A)
        }
    }
    fun onSwipeRight():EventHandler<SwipeEvent>{
        return EventHandler{
//            X2048Service.processCode(KeyCode.RIGHT)
            X2048Service.processCode(KeyCode.D)
        }
    }
    fun onSwipeUp():EventHandler<SwipeEvent>{
        return EventHandler{
//            X2048Service.processCode(KeyCode.UP)
            X2048Service.processCode(KeyCode.W)
        }
    }
    init {
        Platform.runLater {
            X2048Service.implInit(6) }
    }
}

class KeyEventHandler : EventHandler<KeyEvent> {
    override fun handle(event: KeyEvent) {
        kp.value="keypressed:${event.code.getName()}"
        when(event.eventType){
            KeyEvent.KEY_PRESSED -> X2048Service.onKeyPressed(event)
            KeyEvent.KEY_RELEASED -> X2048Service.onKeyReleased(event)
        }
    }
}
class MouseEventHandler: EventHandler<MouseEvent> {
    override fun handle(event: MouseEvent) {
        if (event.button === MouseButton.PRIMARY) {
            when(X2048Service.mMousePressed){
                true ->  {
                    X2048Service.mMousePressed=false
                    X2048Service.mReleasedPoint=X2048Service.Point(event.x.toInt(), event.y.toInt())
                    X2048Service.mouseManipulation()
                }
                false ->  {
                    X2048Service.mMousePressed=true
                    X2048Service.mPressedPoint=X2048Service.Point(event.x.toInt(), event.y.toInt())
                }
            }
        }
    }
}
