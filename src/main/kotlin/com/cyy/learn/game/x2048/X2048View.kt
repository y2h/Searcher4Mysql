package com.cyy.learn.game.x2048

import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.geometry.Orientation
import javafx.scene.control.Button
import javafx.scene.control.Slider
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import tornadofx.*
import kotlin.math.roundToLong

lateinit var sliderSize: Slider
lateinit var playArea: Pane
lateinit var btnReset: Button
val tbScore = SimpleStringProperty("0")
var mLastSize: Long = 4
val kp= SimpleStringProperty("a")
class X2048View : View("X2048") {
    val C: X2048Controller by inject()

    override val root = borderpane {
        prefHeight=571.0
        prefWidth=728.0
        top = hbox(10) {
            label(kp)
            label("得分：")
            label(tbScore)
            label("大小:")
            sliderSize = slider(3.0, 8.0, 4.0, Orientation.HORIZONTAL) {
                blockIncrement = 1.0
                majorTickUnit = 1.0
                minorTickCount = 1
                isShowTickLabels = true
                valueProperty().addListener { _, oldValue, newValue ->
                    val o = Math.round(oldValue.toDouble())
                    val n = Math.round(newValue.toDouble())
                }
                onMouseReleased = EventHandler<MouseEvent> {
                    val n = sliderSize.value.roundToLong()
                    sliderSize.value = n.toDouble()
                    if (n != mLastSize) {
                        mLastSize = n
                        X2048Service.implInit(n.toInt())
                    }
                    playArea.requestFocus()
                }

            }
            btnReset = button("重置") {
                focusedProperty().addListener { _, _, newValue ->
                    if (newValue) {
                        playArea.requestFocus()
                    }
                }
                action {
                    C.onReset()
                }
            }
        }
        center = pane {
            playArea = this
            onMousePressed = MouseEventHandler()
            onMouseReleased = MouseEventHandler()
            onSwipeDown = C.onSwipeDown()
            onSwipeLeft = C.onSwipeLeft()
            onSwipeRight = C.onSwipeRight()
            onSwipeUp = C.onSwipeUp()

        }
        paddingAll=10.0
        onKeyPressed=KeyEventHandler()
        onKeyReleased=KeyEventHandler()
    }
    init {
    }
}

