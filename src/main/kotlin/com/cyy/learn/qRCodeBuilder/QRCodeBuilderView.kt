package com.cyy.learn.qRCodeBuilder

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.ImageView
import javafx.scene.paint.Color
import tornadofx.*

val contentTextField = SimpleStringProperty("")
val codeFormat = SimpleStringProperty("utf-8")
val codeFormats = listOf("utf-8", "gb2312", "ISO-8859-1", "US-ASCII", "utf-16")

val marginChoice = SimpleIntegerProperty(1)
val marginChoices = listOf(1, 2, 3, 4)

val imageFormat = SimpleStringProperty("png")
val imageFormats = listOf("png", "jpg", "gif", "jpeg", "bmp")
val errorCorrectionLevel = SimpleObjectProperty<ErrorCorrectionLevel>(ErrorCorrectionLevel.H)
val errorCorrectionLevels = listOf(ErrorCorrectionLevel.L, ErrorCorrectionLevel.M,
        ErrorCorrectionLevel.Q, ErrorCorrectionLevel.H)
val logoCheck = SimpleBooleanProperty()
val logoSlider = SimpleIntegerProperty()
val frontColor = SimpleObjectProperty(Color.BLACK)
val backColor = SimpleObjectProperty(Color.WHITE)
lateinit var codeImageView: ImageView
lateinit var codeImageView1: ImageView

class QRCodeBuilderView : View("QRCodeBuilder") {
    val C: QRCodeBuilderController by inject()

    override val root = borderpane {
        paddingAll = 10.0
        top = vbox(5) {
            hbox(5) {
                label("地址：")
                textfield(contentTextField) { promptText = "null" }
                choicebox(codeFormat, codeFormats)
                button("生成") {
                    action {
                        C.builderAction()
                    }
                }
                button("加载logo") {
                    action {
                        C.logoAction()
                    }
                }
                button("截图识别") {
                    action {
                        C.snapshotAction()
                    }
                }
                button("识别桌面") {
                    action {
                        C.snapshotDesktopAction()
                    }
                }
                button("保存") {
                    action {
                        C.saveAction()
                    }
                }
            }
            hbox(5) {
                label("前景色：")
                colorpicker(frontColor)
                label("背景色：")
                colorpicker(backColor)
                label("容错等级：")
                choicebox(errorCorrectionLevel, errorCorrectionLevels)
                label("边距：")
                choicebox(marginChoice, marginChoices)
                label("图片格式：")
                choicebox(imageFormat, imageFormats)
                checkbox("Logo", logoCheck) { isSelected = true }
            }
            hbox(5) {
                label("Logo大小")
                slider(0, 100, 15) {
                    isShowTickLabels = true
                    isShowTickMarks = true
                    valueProperty().bindBidirectional(logoSlider)
                }
            }
        }
        center = hbox(5) {

            imageview() {
                fitHeight = 320.0
                fitWidth = 320.0
                isPickOnBounds = true
                isPreserveRatio = true
                codeImageView=this
            }
            imageview() {
                fitHeight = 320.0
                fitWidth = 320.0
                isPickOnBounds = true
                isPreserveRatio = true
                codeImageView1=this
            }

        }
    }
}