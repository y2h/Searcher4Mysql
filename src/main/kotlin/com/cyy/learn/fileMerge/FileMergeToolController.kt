package com.cyy.learn.fileMerge

import javafx.stage.FileChooser
import tornadofx.*
import java.io.File
import java.util.ArrayList
import javax.swing.filechooser.FileSystemView
import com.cyy.util.FileChooserUtil
import com.cyy.util.TooltipUtil
import javafx.application.Platform


class FileMergeToolController : Controller() {
    fun selectFileAction(){
        var files: List<File>? = null
        try {
            val fileChooser = FileChooser()
            fileChooser.title = "请选择文件"
            fileChooser.initialDirectory = FileSystemView.getFileSystemView().homeDirectory
            files = fileChooser.showOpenMultipleDialog(null)
        } catch (e: NullPointerException) {
            Error("选择文件错误", e)
        }

        if (files != null) {
            val strings = ArrayList<String>()
            for (file in files!!) {
                strings.add(file.path)
            }
            selectFile.text = strings.joinToString("|")
        }
    }
    fun selectFolderAction(){
        val file = FileChooserUtil.chooseDirectory()
        if (file != null) {
            selectFile.text = file.path
        }
    }
    fun mergeAction(){
        TooltipUtil.showToast("正在解析请稍后...")
        Thread {
            try {
                fileMergeToolService.mergeAction()
                log.info("解析完成。")
                Platform.runLater { TooltipUtil.showToast("解析完成。") }
            } catch (e: Exception) {
//                log.error("解析失败：", e)
                Error("解析失败：", e)
            }
        }.start()
    }
    fun saveFilePathAction(){
        val file = FileChooserUtil.chooseDirectory()
        if (file != null) {
            saveFilePath.text = file.path
        }
    }
}