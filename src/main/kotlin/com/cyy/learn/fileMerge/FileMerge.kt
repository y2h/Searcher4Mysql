package com.cyy.learn.fileMerge

import com.cyy.util.FileChooserUtil
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.CheckBox
import javafx.scene.control.ChoiceBox
import javafx.scene.control.TextField
import javafx.scene.control.ToggleGroup
import javafx.scene.layout.Priority
import tornadofx.*

val selectFileTextField = SimpleStringProperty("")
val saveFilePathTextField = SimpleStringProperty("")
val selectedFileType = SimpleStringProperty("文件")

val fileType = SimpleStringProperty("")
lateinit var selectFile: TextField
lateinit var saveFilePath: TextField
lateinit var fileTypeChoiceBox: ChoiceBox<String>
lateinit var includeHandCheckBox: CheckBox
lateinit var tgFileType: ToggleGroup

class FileMerge : View("FileMergeTool") {
    val C: FileMergeToolController by inject()
    override val root = anchorpane {
        vbox(10) {
            prefHeight = 400.0
            prefWidth = 1024.0
            paddingAll = 10.0

            label("文件选择：")
            selectFile = textfield(selectFileTextField) {
                promptText = "可选择目录或多个文件"
                hgrow = Priority.ALWAYS
            }
            hbox(5) {
                button("选择文件") {
                    action {
                        C.selectFileAction()
                    }
                }
                button("选择文件夹") {
                    action {
                        C.selectFolderAction()
                    }
                }
//                fileTypeChoiceBox = choicebox(fileType, listOf("Excel", "CSV", "文件")) {
//                    selectionModel.select(2)
//                }
                togglegroup {
                    radiobutton("Excel") {
                        userData = text

                    }
                    radiobutton("CSV") { userData = text }
                    radiobutton("文件") {
                        userData = text
                        isSelected = true
                    }
                    selectedToggleProperty().addListener { _, _, newValue ->
                        selectedFileType.value = newValue.userData.toString()
                    }
                }
            }

            vbox(5) {
                label("文件合并方式：")
                includeHandCheckBox = checkbox("包含标题行") { isSelected = true }
            }

            hbox(5) {
                label("输出文件夹：")
                button("选择") {
                    action {
                        C.saveFilePathAction()
                    }
                }
                saveFilePath = textfield(saveFilePathTextField) {
                    promptText = "留空为原文件同目录"
                    hgrow = Priority.ALWAYS
                }
            }

            button("开始合并") {
                action {
                    C.mergeAction()
                }
            }
            label("Sheet选择：")
            val sheetSelectTextField = SimpleStringProperty("")
            textfield(sheetSelectTextField) {
                //                tooltip.text="sheet从0开始，多个组合可用,分割"
            }
        }
    }

    init {
        FileChooserUtil.setOnDrag(selectFile, FileChooserUtil.FileType.FOLDER);
        FileChooserUtil.setOnDrag(saveFilePath, FileChooserUtil.FileType.FOLDER);
    }
}
