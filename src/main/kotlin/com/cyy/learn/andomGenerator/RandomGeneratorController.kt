package com.cyy.learn.andomGenerator

import cn.hutool.core.util.RandomUtil
import tornadofx.*
import java.util.*

class RandomGeneratorController : Controller() {
    fun generateUUID(){
        uuidResult.value=UUID.randomUUID().toString().trim().replace("-", "")
    }
    fun generateNumber(floor:Int,ceil:Int):String{
        return RandomUtil.randomInt(floor, ceil).toString()
    }
    fun generateEmail():String{
        return RandomUtil.randomString(RandomUtil.randomInt(3, 10)) + "@" + RandomUtil.randomString(RandomUtil.BASE_CHAR,
                RandomUtil.randomInt(3, 5)) + "." + RandomUtil.randomString(RandomUtil.BASE_CHAR, RandomUtil.randomInt(1, 5))
    }
    fun generateLowerCase(length:Int):String{
        return RandomUtil.randomString(RandomUtil.BASE_CHAR,length)
    }
    fun generateUpperCase(length:Int):String{
        return RandomUtil.randomString(RandomUtil.BASE_CHAR,length).toUpperCase()
    }
    fun generateLetter(length:Int):String{
        return RandomUtil.randomString(RandomUtil.BASE_CHAR + RandomUtil.BASE_CHAR.toUpperCase(), length)
    }
    fun generateString(length:Int):String{
        return RandomUtil.randomString(length)
    }
    fun generateText(length:Int):String{
        return RandomUtil.randomString(RandomUtil.BASE_CHAR + RandomUtil.BASE_CHAR.toUpperCase() + "~!@#$%^&*()_+{}:\"<>?`-=[];'\\|,./", length)
    }
}