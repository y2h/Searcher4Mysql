package com.cyy.learn.andomGenerator

import javafx.beans.property.SimpleStringProperty
import javafx.scene.layout.Priority
import tornadofx.*

val uuidResult = SimpleStringProperty("")
val ceil = SimpleStringProperty("")
val ignoreRange = SimpleStringProperty("")
val precision = SimpleStringProperty("")
val numberResult = SimpleStringProperty("")
val emailResult = SimpleStringProperty("")
val lowerCaseLength = SimpleStringProperty("")
val lowerCaseResult = SimpleStringProperty("")
val upperCaseLength = SimpleStringProperty("")
val upperCaseResult = SimpleStringProperty("")
val letterLength = SimpleStringProperty("")
val letterResult = SimpleStringProperty("")
val stringLength = SimpleStringProperty("")
val stringResult = SimpleStringProperty("")
val textLength = SimpleStringProperty("")
val textResult = SimpleStringProperty("")
val selectedEmailHost = SimpleStringProperty("")

val floor = SimpleStringProperty("")

class RandomGeneratorView : View("RandomGeneratorTool") {
    val C: RandomGeneratorController by inject()

    override val root = vbox(5) {
        paddingAll = 10.0
        hbox(5) {
            label("UUID")
            button("生成") {
                action {
                    C.generateUUID()
                }
            }
            textfield(uuidResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }
        }

        hbox(5) {
            label("数字")
            textfield(floor) { promptText = "下限" }
            textfield(ceil) { promptText = "上限" }
            textfield(ignoreRange) { promptText = "忽略区间, 格式: 0-3,5-7" }
            textfield(precision) { promptText = "精度" }
            button("生成") {
                action {
                    numberResult.value = C.generateNumber(floor.value.toInt(), ceil.value.toInt())
                }
            }
            textfield(numberResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }

        }
        hbox(5) {
            label("邮箱")
            togglegroup {
                radiobutton("126") {
                    userData = text

                }
                radiobutton("163") { userData = text }
                radiobutton("qq") {
                    userData = text
                    isSelected = true
                }
                selectedToggleProperty().addListener { _, _, newValue ->
                    selectedEmailHost.value = newValue.userData.toString()
                }
            }
            button("生成") {
                action {
                    emailResult.value = C.generateEmail()
                }
            }
            textfield(emailResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }

        }
        hbox(5) {
            label("小写字母")
            textfield(lowerCaseLength) { promptText = "长度" }
            button("生成") {
                action {
                    lowerCaseResult.value = C.generateLowerCase(lowerCaseLength.value.toInt())
                }
            }
            textfield(lowerCaseResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }

        }
        hbox(5) {
            label("大写字母")
            textfield(upperCaseLength) { promptText = "长度" }
            button("生成") {
                action {
                    upperCaseResult.value = C.generateUpperCase(upperCaseLength.value.toInt())
                }
            }
            textfield(upperCaseResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }

        }
        hbox(5) {
            label("字母（包含大小写）")
            textfield(letterLength) { promptText = "长度" }
            button("生成") {
                action {
                    letterResult.value = C.generateLetter(letterLength.value.toInt())
                }
            }
            textfield(letterResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }

        }
        hbox(5) {
            label("无符号字符（只有字母和数字）")
            textfield(stringLength) { promptText = "长度" }
            button("生成") {
                action {
                    stringResult.value = C.generateString(stringLength.value.toInt())
                }
            }
            textfield(stringResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }

        }
        hbox(5) {
            label("字符（包含字母/数字/符号）")
            textfield(textLength) { promptText = "长度" }
            button("生成") {
                action {
                    textResult.value = C.generateText(textLength.value.toInt())
                }
            }
            textfield(textResult) {
                promptText = "结果"
                hgrow = Priority.ALWAYS
            }
        }
    }
}