package com.cyy.learn.mp3Convert

import tornadofx.*


/**
 * @ClassName: Mp3ConvertToolController
 * @Description: mp3格式转换工具
 * @author: xufeng
 * @date: 2019/8/8 0008 20:41
 */


class Mp3ConvertToolController : Controller() {
    fun addFileAction(){
    }
    fun addFolderAction(){
    }
    fun convertAction(){
    }
    fun outputFolderAction(){
    }
    fun openOutputFolderAction(){
    }

//    private val mp3ConvertToolService = Mp3ConvertToolService(this)
//    private val tableData = FXCollections.observableArrayList<Map<String, String>>()//表格数据
//

//
//    private fun initView() {
//        JavaFxViewUtil.setTableColumnMapValueFactory(fileNameTableColumn, "fileName", false)
//        JavaFxViewUtil.setTableColumnMapValueFactory(absolutePathTableColumn, "absolutePath", false)
//        JavaFxViewUtil.setTableColumnMapValueFactory(fileSizeTableColumn, "fileSize", false)
//        JavaFxViewUtil.setTableColumnMapValueFactory(convertStatusTableColumn, "convertStatus", false)
//        tableViewMain.setItems(tableData)
//    }
//
//    private fun initEvent() {
//        FileChooserUtil.setOnDrag(outputFolderTextField, FileChooserUtil.FileType.FOLDER)
//        tableViewMain.setOnMouseClicked({ event ->
//            if (event.getButton() === MouseButton.SECONDARY) {
//                val menu_Remove = MenuItem("移除选中行")
//                menu_Remove.setOnAction { event1 -> tableData.remove(tableViewMain.getSelectionModel().getSelectedItem()) }
//                val menu_RemoveAll = MenuItem("移除所有")
//                menu_RemoveAll.setOnAction { event1 -> tableData.clear() }
//                tableViewMain.setContextMenu(ContextMenu(menu_Remove, menu_RemoveAll))
//            }
//        })
//    }
//
//    private fun initService() {}
//
//    @FXML
//    @Throws(Exception::class)
//    private fun addFileAction(event: ActionEvent) {
//        val file = FileChooserUtil.chooseFile()
//        if (file != null) {
//            mp3ConvertToolService.addTableData(file)
//        }
//    }
//
//    @FXML
//    @Throws(Exception::class)
//    private fun addFolderAction(event: ActionEvent) {
//        val folder = FileChooserUtil.chooseDirectory()
//        if (folder != null) {
//            FileUtils.listFiles(folder, null, false).forEach { file: File -> mp3ConvertToolService.addTableData(file) }
//        }
//    }
//
//    @FXML
//    private fun convertAction(event: ActionEvent) {
//        mp3ConvertToolService.convertAction()
//    }
//
//    @FXML
//    private fun outputFolderAction(event: ActionEvent) {
//        val folder = FileChooserUtil.chooseDirectory()
//        if (folder != null) {
//            outputFolderTextField.setText(folder.path)
//        }
//    }
//
//    @FXML
//    private fun openOutputFolderAction(event: ActionEvent) {
//        if (StringUtils.isNotEmpty(outputFolderTextField.getText())) {
//            XJavaFxSystemUtil.openDirectory(outputFolderTextField.getText())
//        }
//    }
}