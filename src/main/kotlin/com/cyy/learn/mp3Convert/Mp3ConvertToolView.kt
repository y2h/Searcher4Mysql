package com.cyy.learn.mp3Convert

import javafx.beans.property.SimpleStringProperty
import tornadofx.*

val outputFolderTextField = SimpleStringProperty("")

class Mp3ConvertToolView : View("Mp3ConvertTool") {
    val C: Mp3ConvertToolController by inject()

    override val root = borderpane {
        center=vbox {  }
        bottom = hbox(10) {
            button("添加文件") {
                action {
                    C.addFileAction()
                }
            }
            button("添加文件夹") {
                action {
                    C.addFolderAction()
                }
            }
            button("一键转换") {
                action {
                    C.convertAction()
                }
            }
            label("输出文件夹：")
            textfield(outputFolderTextField) { promptText = "默认为原目录" }
            button("选择") {
                action {
                    C.outputFolderAction()
                }
            }
            button("打开输出文件夹") {
                action {
                    C.openOutputFolderAction()
                }
            }
        }
        top{
            label("目前支持.ncm、.qmc转换为mp3格式")
        }
    }
}