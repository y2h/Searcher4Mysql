package com.cyy.app

import cn.hutool.core.io.resource.ResourceUtil.getResource
import com.cyy.learn.oreilly_foo_many_types_of_ai_demo.tsp_europe.TSPView
import com.cyy.learn.oreilly_foo_many_types_of_ai_demo.MainDashboard
import com.cyy.algovis.分形图V
import com.cyy.algovis.蒙特卡洛算法求Pi
import com.cyy.algovis.选择排序V
import com.cyy.learn.andomGenerator.RandomGeneratorView
import com.cyy.learn.asciiPicTool.AsciiPicTool
import com.cyy.learn.fileMerge.FileMerge
import com.cyy.learn.fileRename.FileRenameV
import com.cyy.learn.javaFxXmlToObjectCode.JavaFxXmlToObjectCode
import com.cyy.learn.jsonConvert.JsonConvert
import com.cyy.learn.合并Excel
import com.cyy.model.Conss
import com.cyy.model.GmodelModel
import com.cyy.view.*
import com.cyy.view.learn.*
import com.cyy.view.tools.UploadFrag
import javafx.scene.Scene
import javafx.stage.Stage
import tornadofx.*

fun main()= launch<MyApp>()
class MyApp : App(MyWorkspace::class, Styles::class) {
    companion object{
        lateinit var stage: Stage
    }

    override fun onBeforeShow(view: UIComponent) {
        workspace.dock<Tools>()
        workspace.dock<JavaFxXmlToObjectCode>()
//        workspace.dock<MergeExcel>()
    }

    val gmodel: GmodelModel by inject()
    override fun start(primaryStage: Stage) {
        super.start(primaryStage)
        primaryStage.width = 1024.0
        primaryStage.height = 768.0

//        stage.scene= Scene(RichText().root)
////        stage.setScene(Scene(RichText().root))
        primaryStage.scene.stylesheets.add(getResource("java-keywords.css").toExternalForm())
        stage=primaryStage
    }

    override fun stop() {
        if (gmodel.dataSource.value != null) {
            try {
                Conss.closeDb(gmodel.dataSource.value)
                gmodel.arp.value.stop()
            } catch (e: Exception) {
                println("some error happen when app stop : ${e}")
            } finally {
                super.stop()
            }
        }
    }
}

class MyWorkspace : Workspace("Tools4Happy",NavigationMode.Tabs) {
    override fun onDock() {
        with (workspace) {
            button("Action").action {
            }
        }
    }
    init {
        menubar {
            menu("File") {
                item("重命名","Shortcut+R").action {
                    dock<FileRenameV>()
                }
                item("文件合并","Shortcut+M").action {
                    dock<FileMerge>()
                }
                menu("Connect") {
                    item("Facebook").action { println("Connecting Facebook!") }
                    item("Twitter").action { println("Connecting Twitter!") }
                }
                item("Save","Shortcut+S").action {
                    println("Saving!")
                }
                item("Quit","Shortcut+Q").action {
                    println("Quitting!")
                }
            }
            menu("Office") {
                item("合并Excel","Shortcut+C").action {
                    dock<合并Excel>()
                }
                item("魔板GUI","Shortcut+V").action {
                    dock<魔板GUI>()
                }
            }
            menu("Learn") {
                item("rowExpander Table","Shortcut+T").action {
                    dock<ExpanderTable>()
                }
                item("SlideShow","Shortcut+S").action {
                    dock<SlideShowView>()
                }
                item("ListNav").action {
                    dock<ListNavView>()
                }
                item("Moving").action {
                    dock<MovingView>()
                }
                item("Treeview").action {
                    dock<TreeviewDemo>()
                }
                item("Animation").action {
                    dock<AnimationDemo>()
                }
                item("ViewAndFrags").action {
                    dock<ViewAndFragsDemoView>()
                }
                item("DataDrivenTable").action {
                    dock<DataDrivenTableRowStyleView>()
                }
                item("Dragging").action {
                    dock<DraggingView>()
                }
                item("Journal").action {
                    dock<JournalView>()
                }
                item("RichText").action {
                    dock<RichText>()
                }
                item("OtherFrag").action {
                    dock<OtherFrag>()
                }
                item("RecursiveCallTree").action {
                    dock<RecursiveCallTree>()
                }
                item("UploadFrag").action {
                    dock<UploadFrag>()
                }
                item("drawer").action {
                    dock<BottomFrag>()
                }
                item("主从表").action {
                    dock<PersonMainView>()
                }
            }

            menu("AlgoVis"){
                item("求PI值").action {
                    dock<蒙特卡洛算法求Pi>()
                }
                item("选择排序").action {
                    dock<选择排序V>()
                }
                item("分形图").action {
                    dock<分形图V>()
                }
            }
            menu("AI"){
                item("TSPView").action {
                    dock<TSPView>()
                }
                item("MachineLearning").action {
                    dock<MainDashboard>()
                }
            }
            menu("开发工具"){
                item("JsonConvert").action {
                    dock<JsonConvert>()
                }
                item("AsciiPicTool").action {
                    dock<AsciiPicTool>()
                }
                item("JavaFxXmlToObjectCode").action {
                    dock<JavaFxXmlToObjectCode>()
                }
            }
            menu("编码工具"){
                item("RandomGenerator").action {
                    dock<RandomGeneratorView>()
                }
            }
        }
        with(leftDrawer) {
            item("文件工具") {
                paddingAll=10.0
                spacing=10.0
                button("重命名") {
                    action {
                        dock<FileRenameV>()
                    }
                }
                button("文件合并") {
                    action {
                        dock<FileMerge>()
                    }
                }
            }
            item("开发工具") {
                paddingAll=10.0
                spacing=10.0
                button("JsonConvert") {
                    action {
                        dock<JsonConvert>()
                    }
                }
                button("AsciiPicTool") {
                    action {
                        dock<AsciiPicTool>()
                    }
                }
                button("JavaFxXmlToObjectCode") {
                    action {
                        dock<JavaFxXmlToObjectCode>()
                    }
                }
            }
            item("编码工具") {
                paddingAll=10.0
                spacing=10.0
                button("RandomGenerator") {
                    action {
                        dock<RandomGeneratorView>()
                    }
                }
                button("AsciiPicTool") {
                    action {
                        dock<AsciiPicTool>()
                    }
                }
                button("JavaFxXmlToObjectCode") {
                    action {
                        dock<JavaFxXmlToObjectCode>()
                    }
                }
            }
            item("Office") {
                vbox {
                    button("合并Excel") { }.action { dock<合并Excel>() }
                    button("魔板GUI") { }.action { dock<魔板GUI>() }
                }
            }
            item("魔板GUI") {
            }
        }
        with(rightDrawer) {
            item("Screencasts") {
                webview {
                    prefWidth = 470.0
                }
            }
            item("Links") {

            }
            item("People") {

            }
        }
        with(bottomDrawer) {
            item("Screencasts") {
                webview {
                    prefWidth = 470.0
                }
            }
            item("Links") {

            }
            item("People") {

            }
        }
    }
}