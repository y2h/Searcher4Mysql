package com.cyy.app

import javafx.geometry.Pos
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val tackyButton by cssclass()
        val jsonConvertBtn by cssclass()
        val heading by cssclass()
        val txt100 by cssclass()
        val main by cssclass()
        val top by cssclass()
        val transparentLayer by cssclass()
        val gameboard by cssclass()
        private val topColor = c("#FF0000")
        private val rightColor = c("#006400")
        private val bottomColor = c("#FFA500")
        private val leftColor = c("#800080")
    }

    init {
        gameboard {
            padding = box(10.px)
        }
        main {
            backgroundColor += c("#361F27")
        }

        top {
            backgroundColor += c("#DD7549")
            padding = box(20.px)
        }

        transparentLayer {
            backgroundColor += c("#222")
            opacity = 0.0
        }

        button {
            padding = box(10.px)
            alignment = Pos.CENTER
            backgroundColor += c("#DD7549")
            fontWeight = FontWeight.EXTRA_BOLD

            and (hover) {
                backgroundColor += c("#A05434")
                textFill = Color.WHITE
            }
            fontWeight = FontWeight.EXTRA_BOLD
        }

        s(listCell, listCell and even, listCell and odd,
                listCell and selected) {
//            backgroundColor += Color.TRANSPARENT
//            backgroundColor += Color.BLUE
        }

        cell {
//            backgroundColor += c("#39393A")
//            textFill = Color.WHITE
        }

        listView {
            and (selected) {
//                backgroundColor += Color.TRANSPARENT
                backgroundColor += Color.BLUE
            }
//            backgroundColor += c("#39393A")
            fitToWidth = true
        }

        tabPane {
            tabHeaderBackground {
                opacity = 0.0
            }
            tabContentArea {
                borderColor += box(c("#DD7549"))
//                borderWidth += box(10.px)
            }
        }

        tab {
//            backgroundColor += c("#87462B")
            fontWeight = FontWeight.EXTRA_BOLD
            and (selected) {
                backgroundColor += c("#DD7549")
                textFill = Color.BLUE
//                borderColor += box(c("#DD7549"))
            }
        }
        label and heading {
//            padding = box(10.px)
            fontSize = 16.px
            fontWeight = FontWeight.BOLD
        }
        tackyButton {
//            rotate = 10.deg
            borderColor += box(topColor,rightColor,bottomColor,leftColor)
            fontFamily = "Comic Sans MS"
            fontSize = 20.px
        }
        jsonConvertBtn{
            minWidth=100.px
        }
        txt100{
            prefWidth=100.px
        }
    }
}

class DDTRS_Styles : Stylesheet() {

    companion object {
        val overdue by cssclass()
        val highlighted by cssclass()
    }

    init {
        tableView {
            tableRowCell {
                and( highlighted ) {
                    backgroundColor += c("yellow")
                }
            }
        }

        overdue {
            textFill = c("red")
            fill = c("blue")
        }
    }
}

class DraggingStyles : Stylesheet(){
    companion object {
        val wrapper by cssclass()
        val toolboxItem by cssclass()
        val workAreaSelected by cssclass()
        val workArea by cssclass()
    }

    init {
        wrapper {
            backgroundColor += Color.WHITE
        }
        workArea {
            backgroundColor += Color.LIGHTGRAY
            borderColor += box(Color.BLACK)
            borderWidth += box(1.px)
        }
        toolboxItem {
            padding = box(4.px)
            stroke = Color.BLACK
            strokeWidth = 1.px
            and(hover) {
                opacity = 0.7
            }
        }
        workAreaSelected {
            borderColor += box(Color.BLACK)
            borderWidth += box(3.px)
        }
    }
}

class SelectingStyles : Stylesheet() {

    companion object {
        val workArea by cssclass()
        val selected by cssclass()
        val circleItem by cssclass()
        val lassoRect by cssclass()
    }

    init {
        workArea {
            backgroundColor += Color.LIGHTGRAY
            borderColor += box(Color.BLACK)
            borderWidth += box(1.px)
        }

        selected {
            stroke = Color.BLACK
            strokeWidth = 2.px
        }

        circleItem {
            hover {
                fill = Color.WHITE
            }
        }

        lassoRect {
            fill = Color.TRANSPARENT
            stroke = Color.BLACK
            strokeWidth = 1.px
        }
    }
}
