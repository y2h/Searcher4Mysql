package com.cyy.view

import com.cyy.model.GmodelModel
import com.cyy.model.SqlHistory
import com.cyy.srv.GenSrv
import javafx.scene.layout.Priority
import tornadofx.*

class 魔板GUI : View("魔板GUI") {
    val genSrv: GenSrv by inject()
    val gmodel: GmodelModel by inject()

    override val root = borderpane {
        //        left<DBView>()
        center{
            add(MainTap(gmodel,genSrv))
        }

        top {
            add(TopView(gmodel, genSrv))
        }
        bottom{
            add(StatusView())
        }

        //        add(form)
//        separator()
//
//        top<TopView>()

        prefWidth = 1000.0
        prefHeight = 700.0
        paddingAll = 10
        vgrow = Priority.ALWAYS
        hgrow=Priority.ALWAYS
//        alignment = Pos.CENTER
    }
}

class GenEvent(val message: String) : FXEvent()
class SaveSqlEvent(var sqlMap: List<SqlHistory>) : FXEvent()