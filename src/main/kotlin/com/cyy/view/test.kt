package com.cyy.view

import javafx.application.Application
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

val mousePressedName = SimpleStringProperty()
fun main() = Application.launch(TestApp::class.java)
class TestApp : App(TestView::class)
class TestView : View("Learn") {

    override val root = vbox(10) {
        prefHeight = 200.0
        prefWidth = 600.0
        alignment = Pos.CENTER
        label(mousePressedName) {
            style {
                fontSize = 50.px
                fontWeight = FontWeight.EXTRA_BOLD
                textFill = Color.RED
            }
        }
        this.onMousePressed = MouseEventHandler1()
        this.onMouseReleased = MouseEventHandler1()
    }
}

class MouseEventHandler1: EventHandler<MouseEvent> {
    override fun handle(event: MouseEvent) {
        mousePressedName.value="${event.button.name} pressed"
    }
}