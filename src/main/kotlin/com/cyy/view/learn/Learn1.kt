package com.cyy.view.learn

import com.cyy.view.RecursiveCallTree
import javafx.collections.FXCollections
import javafx.geometry.Pos
import tornadofx.*
import javafx.geometry.Side
import javafx.scene.text.FontWeight
import tornadofx.WizardStyles.Companion.graphic

class OtherFrag : Fragment() {
    override val root = drawer(side = Side.RIGHT, multiselect = true) {
        item("squeezebox", expanded = false) {
            add(
                    squeezebox {
                        fold("Customer Editor", expanded = true) {
                            form {
                                fieldset("Customer Details") {
                                    field("Name") { textfield() }
                                    field("Password") { textfield() }
                                }
                            }
                        }
                        fold("Some other editor", expanded = true) {
                            stackpane {
                                label("Nothing here")
                            }
                        }
                    }
            )
        }

        item("squeezebox1", expanded = false) {
            add(
                    squeezebox {
                        fold("Customer Editor", expanded = true, closeable = true) {
                            form {
                                fieldset("Customer Details") {
                                    field("Name") { textfield() }
                                    field("Password") { textfield() }
                                }
                            }
                        }
                        fold("Some other editor", closeable = true) {
                            stackpane {
                                label("Nothing here")
                            }
                        }
                    }
            )
        }

        item("squeezebox2", expanded = false) {
            add(ListView::class)
        }
    }
}

class ListView : View() {
    val responses = FXCollections.observableArrayList<Student>()
    override val root = vbox {
        listview(responses) {
            cellFormat {
                graphic = cache {
                    form {
                        fieldset {
                            label(it.name) {
                                alignment = Pos.CENTER_RIGHT
                                style {
                                    fontSize = 22.px
                                    fontWeight = FontWeight.BOLD
                                }
                            }
                            field("address:") {
                                label(it.address)
                            }
                        }
                    }
                }
                onUserSelect(1) {
                    //                    println(it)
                }
            }
        }
        var id = 0
        button("Add Item").action {
            responses.add(Student("Student${id}",
                    "address${id}"))
            id++
        }
    }
}

class Student(val name: String, val address: String) {
    override fun toString(): String {
        return "Student(name=${name},address=${address})"
    }
}