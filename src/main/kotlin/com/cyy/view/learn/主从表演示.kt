package com.cyy.view.learn

import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import tornadofx.*

class Person(name: String, cars: List<Car>) {
    val nameProperty = stringProperty(name)
    var name by nameProperty

    val carsProperty = listProperty<Car>(FXCollections.observableArrayList(cars))
    var cars by carsProperty
}

class PersonModel : ItemViewModel<Person>() {
    val name = bind(Person::nameProperty)
    val cars: SimpleListProperty<Car> = bind(Person::carsProperty)
}

class Car(brand: String, model: String) {
    val brandProperty = stringProperty(brand)
    var brand by brandProperty

    val modelProperty = stringProperty(model)
    var model by modelProperty
}

class CarModel(car: Car? = null) : ItemViewModel<Car>(car) {
    val brand = bind(Car::brandProperty)
    val model = bind(Car::modelProperty)
}

class DataController : Controller() {
    val people = FXCollections.observableArrayList<Person>()

    init {
        people.addAll(
                Person("Person 1", listOf(Car("BMW", "M3"), Car("Ford", "Fiesta"))),
                Person("Person 2", listOf(Car("长安", "奔奔"), Car("哈弗", "H6")))
        )
    }
}

class PersonMainView : View("主从表演示") {
    val data: DataController by inject()
    val selectedPerson: PersonModel by inject()

    override val root = borderpane {
        center {
            tableview(data.people) {
                column("Name", Person::nameProperty)
                bindSelected(selectedPerson)
            }
        }
        right(PersonEditor::class)
    }
}

class PersonEditor : View() {
    val person: PersonModel by inject()
    val selectedCar : CarModel by inject()

    override val root = form {
        fieldset {
            field("Name") {
                textfield(person.name).required()
            }
            field("Cars") {
                tableview(person.cars) {
                    column("Brand", Car::brandProperty)
                    column("Model", Car::modelProperty)
                    bindSelected(selectedCar)
                    onUserSelect(2) {
                        find<CarEditor>().openModal()
                    }
                }
            }
            button("Save") {
                enableWhen(person.dirty)
                action {
                    person.commit()
                }
            }
        }
    }
}

class CarEditor : View() {
    val car: CarModel by inject()
    val person: PersonModel by inject()

    override val root = form {
        fieldset {
            field("Brand") {
                textfield(car.brand).required()
            }
            field("Model") {
                textfield(car.model).required()
            }
            button("Save").action {
                val index = person.cars.indexOf(car.item)

                car.commit {
                    person.cars[index] = car.item
                    close()
                }
            }
        }
    }
}