package com.cyy.view.search

import com.cyy.model.GmodelModel
import javafx.scene.layout.Priority
import javafx.scene.web.WebView
import tornadofx.*

class SearchCenter : View("My View") {
    override val root = scrollpane {
        add(WbView())

        vgrow = Priority.ALWAYS
        hgrow = Priority.ALWAYS
    }
}

class WbView : View() {
    val gm: GmodelModel by inject()
    lateinit var wb: WebView
    override val root = webview {
        wb = this
        dynamicContent(gm.tplStringOut) {
            run {
                this.engine.loadContent(gm.tplStringOut.value)
            }
        }
        engine.loadContent("<h1>Welcome!!!</h1>")
    }
}

