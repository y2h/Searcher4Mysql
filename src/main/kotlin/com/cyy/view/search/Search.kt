package com.cyy.view.search

import com.cyy.model.GmodelModel
import javafx.scene.layout.Priority
import tornadofx.*

class Search(val gm: GmodelModel) : View("My View") {
    override val root = borderpane {
        left(SearchLeftTab::class)

        center{
            add(SearchCenter())
            vgrow = Priority.ALWAYS
            hgrow = Priority.ALWAYS
        }


    }
}



