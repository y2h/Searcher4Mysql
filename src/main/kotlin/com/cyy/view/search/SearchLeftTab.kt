package com.cyy.view.search

import tornadofx.*

class SearchLeftTab : View("My View") {
    override val root = tabpane {
        tab("点击查寻") {
            add(SearchLeft())
        }
        tab("SQL查寻") {
            add(SQLSearchTab())
        }
        tab("查寻历史") {
            add(SearchHisttory())
        }
        tabs.forEach { it.isClosable = false }
    }
}

