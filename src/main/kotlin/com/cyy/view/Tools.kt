package com.cyy.view

import com.cyy.app.MyWorkspace
import com.cyy.learn.andomGenerator.RandomGeneratorView
import com.cyy.learn.asciiPicTool.AsciiPicTool
import com.cyy.learn.characterConverter.CharacterConverterView
import com.cyy.learn.fileMerge.FileMerge
import com.cyy.learn.fileRename.FileRenameV
import com.cyy.learn.game.x2048.X2048View
import com.cyy.learn.javaFxXmlToObjectCode.JavaFxXmlToObjectCode
import com.cyy.learn.jsonConvert.JsonConvert
import com.cyy.learn.mp3Convert.Mp3ConvertToolView
import com.cyy.learn.qRCodeBuilder.QRCodeBuilderView
import com.cyy.learn.shortURL.ShortURLView
import com.cyy.learn.game.sudoku.SudokuView
import tornadofx.*

class Tools : View("Tools") {
    override val root = vbox(10) {
        paddingAll=10.0
        hbox(5) {
            label("文件工具")
            button("重命名") {
                action {
                    find(MyWorkspace::class).dock<FileRenameV>()
                }
            }
            button("文件合并") {
                action {
                    find(MyWorkspace::class).dock<FileMerge>()
                }
            }
        }
        hbox(5) {
            label("开发工具")
            button("JsonConvert") {
                action {
                    find(MyWorkspace::class).dock<JsonConvert>()
                }
            }
            button("AsciiPicTool") {
                action {
                    find(MyWorkspace::class).dock<AsciiPicTool>()
                }
            }
            button("JavaFxXmlToObjectCode") {
                action {
                    find(MyWorkspace::class).dock<JavaFxXmlToObjectCode>()
                }
            }
        }
        hbox(5) {
            label("编码工具")
            button("RandomGenerator") {
                action {
                    find(MyWorkspace::class).dock<RandomGeneratorView>()
                }
            }
        }

        hbox(5) {
            label("Web工具")
            button("网址缩短") {
                action {
                    find(MyWorkspace::class).dock<ShortURLView>()
                }
            }
        }
        hbox(5) {
            label("小工具")
            button("字符转换") {
                action {
                    find(MyWorkspace::class).dock<CharacterConverterView>()
                }
            }
            button("二维码生成") {
                action {
                    find(MyWorkspace::class).dock<QRCodeBuilderView>()
                }
            }
            button("mp3格式转换") {
                action {
                    find(MyWorkspace::class).dock<Mp3ConvertToolView>()
                }
            }
        }
        hbox(5) {
            label("游戏")
            button("2048") {
                action {
                    find(MyWorkspace::class).dock<X2048View>()
                }
            }
            button("数独") {
                action {
                    find(MyWorkspace::class).dock<SudokuView>()
                }
            }
        }
        hbox(5) {
            label("测试")
            button("Test") {
                action {
                    find(MyWorkspace::class).dock<TestView>()
                }
            }
        }
        hbox(5) {
            label("算法")
            button("Test") {
                action {
                    find(MyWorkspace::class).dock<TestView>()
                }
            }
        }

    }
}
