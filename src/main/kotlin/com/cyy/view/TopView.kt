package com.cyy.view

import com.cyy.app.Styles
import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import javafx.geometry.Orientation
import javafx.scene.control.TextFormatter
import tornadofx.*

class TopView(val gmodel: GmodelModel, val genSrv: GenSrv) : View("My View") {
    val engine = gmodel.engine.value
    // 限定textfield输入值
    val FirstTenFilter: (TextFormatter.Change) -> Boolean = { change ->
        !change.isAdded || change.controlNewText.let {
            it.isInt() && it.toInt() in 0..65535
        }
    }

    override val root =titledpane("数据库设置") {
        hbox(5) {
            form {
                fieldset(labelPosition = Orientation.VERTICAL) {
                    hbox(10) {
                        field("主机地址") {
                            textfield(gmodel.host) {
                                addClass(Styles.txt100)
                                text = "localhost"
                                required(message = "Enter host for your database")
                            }
                        }
                        field("主机端口") {
                            textfield(gmodel.port) {
                                addClass(Styles.txt100)
                                text = "3306"
                                filterInput(FirstTenFilter)
                                required(message = "Enter port for your database")
                            }
                        }
                        field("用户名") {
                            textfield(gmodel.user) {
                                addClass(Styles.txt100)
                                text = "root"
                                required(message = "Enter user name for your database")
                            }
                        }
                        field("密码") {
                            textfield(gmodel.pwd) {
                                addClass(Styles.txt100)
                                text = "root"
                                required(message = "Enter user name for your database")
                            }
                        }

                        field("数据库") {
                            combobox(gmodel.dbname, gmodel.dbs) {
                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (!selectionModel.selectedItem.isNullOrEmpty()) {
                                        run {
                                            genSrv.prapare()
                                            gmodel.tableCounts.value = "当前数据库中共有${gmodel.tables.size.toString()}张表"
                                            gmodel.exCols.clear()

                                        }
                                    })
                                }
                            }
                        }
                        field("数据表") {
                            combobox(gmodel.leftTable, gmodel.tables){
                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (!selectionModel.selectedItem.isNullOrEmpty()) {

                                        gmodel.columns.clear()
                                        gmodel.getColumnsSql.value = "select column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='${gmodel.dbname.value}' and TABLE_NAME='${gmodel.leftTable.value}'"
                                        gmodel.columns.addAll(genSrv.getList(gmodel.dataSource.value.connection, gmodel.getColumnsSql.value))
//
                                    })
                                }
                            }
                        }
                    }
                }
                fieldset {
                    field("jdbcUrl") {
                        hbox(10) {
                            textfield(gmodel.jdbcUrl) {
                               prefWidth=400.0
                                required(message = "Enter user name for your database")
                            }
                            button("测试连接") {
                                action {
                                    if ((gmodel.arp.value != null)) {
//                    Conss.closeDb(gmodel.dataSource.value)
                                        gmodel.arp.value.stop()
                                    }
                                    // 每次点击"测试连接"测试连接时，都将之前保存的db和table列表清空
                                    gmodel.dbs.clear()
                                    gmodel.tables.clear()
                                    if (testDb2().not()) return@action
                                    gmodel.dbOK.value = true
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * test connection for postgresql, mysql,oracle,sqlserver
     */
    fun testDb2(): Boolean {
        try {
            genSrv.getCon()
            gmodel.dbOK.value = true
            genSrv.getDBs()
            fire(GenEvent("恭喜！数据库连接成功！"))
            return true
        } catch (e: Exception) {
            gmodel.dbOK.value = false
            fire(GenEvent("testDb() test connection failed: ${e}"))
            return false
        }
    }
}

