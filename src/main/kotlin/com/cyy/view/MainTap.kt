package com.cyy.view

import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import com.cyy.view.search.Search
import tornadofx.*

class MainTap(val gmodel: GmodelModel, val genSrv: GenSrv) : View("My View") {

    override val root = tabpane {
        tab("通用查寻") {
            add(Search(gmodel))
        }
        tab("Enjoy") {
            add(GEnjoy(gmodel,genSrv))
        }
        tab("生成器") {
            add(DBView())
        }
        tabs.forEach{ it.isClosable=false}
    }
}
