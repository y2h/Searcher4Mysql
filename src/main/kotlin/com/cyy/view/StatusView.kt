package com.cyy.view

import com.cyy.model.GmodelModel
import com.cyy.view.learn.BottomFrag
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.layout.Priority
import tornadofx.*

class StatusView() : View() {
    val gm: GmodelModel by inject()
    val vm: StatusViewModel by inject()

    override val root = hbox(10) {
        label{ bind(stringProperty("[db Type]:").concat(gm.dbtype)) }
        separator(Orientation.VERTICAL)
        label{ bind(stringProperty("[db Port]:").concat(gm.port)) }
        separator(Orientation.VERTICAL)
        label{ bind(stringProperty("[db File]:").concat(gm.dbname)) }
        separator(Orientation.VERTICAL)
        label{ bind(stringProperty("[db Driver]:").concat(gm.driver)) }
        separator(Orientation.VERTICAL)
        label { bind(stringProperty("提示：").concat(vm.lastMessage ?:"")) }

        paddingAll = 4
        vgrow = Priority.NEVER
        prefHeight = 10.0
    }
}

class StatusViewModel : ViewModel() {

    val lastMessage = stringProperty()

    init {
        subscribe<GenEvent> {
            lastMessage.value = it.message
        }
    }
}