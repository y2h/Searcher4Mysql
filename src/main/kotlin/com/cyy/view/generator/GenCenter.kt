package com.cyy.view.generator

import com.cyy.controller.CyyGenerator
import com.cyy.model.Conss
import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import com.cyy.view.GenEvent
import com.jfinal.kit.Kv
import com.jfinal.kit.StrKit
import com.jfinal.plugin.activerecord.generator.TypeMapping
import javafx.geometry.Orientation
import javafx.scene.control.SelectionMode
import tornadofx.*

class GenCenter : View("My View") {
    val gmodel: GmodelModel by inject()
    val engine = gmodel.engine.value

    val genSrv:GenSrv by inject()
    override val root = vbox {
        titledpane("目录设置") {
            form {
                prefWidth = 900.0
                hbox(5) {
                    //  数据库列表
                    fieldset(labelPosition = Orientation.VERTICAL) {
                        //                    label(gmodel.tableCounts)
                        field("选择数据库") {
                            listview(gmodel.dbs) {
                                id = "dbList"
                                visibleWhen(gmodel.dbOK)
                                selectionModel.selectionMode = SelectionMode.SINGLE
                                bindSelected(gmodel.dbname)
                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (!selectionModel.selectedItem.isNullOrEmpty()) {
                                        try {
                                            genSrv.prapare()
                                        } catch (e: Exception) {
                                            println("db change failed : ${e}")
                                        }
                                    })
                                }
                            }
                        }
                        field {
                            button("Generate") {
                                enableWhen(gmodel.dbOK)

                                action {
                                    try {
                                        val btp = gmodel.baseTemplatePath.value
//                                        val a = arrayListOf<String>()
//                                        val aa = arrayListOf<String>()
//                                        val c = gmodel.removedTableNamePrefixes.value.split(",")
////                    a.add("ay_")
////                    a.add("Ay_")
//                                        a.addAll(c)
//                                        val removedTableNamePrefixes = a.toTypedArray()
//                                        aa.addAll(gmodel.excludedTable)
//                                        val excludedTable = aa.toTypedArray()
//                                        Conss.setMetaBuilder(gmodel.metaBuilder.value,removedTableNamePrefixes,excludedTable)


//                                        gmodel.metaBuilder.value.setRemovedTableNamePrefixes(*b)
//                                        gmodel.metaBuilder.value.addExcludedTable(*bb)
//                                        gmodel.metaBuilder.value.setTypeMapping(TypeMapping())
                                        gmodel.metaBuilder.value=genSrv.prepareMetaBuilder()
                                        engine.setBaseTemplatePath(btp)
                                        val gen = CyyGenerator(gmodel.dataSource.value, engine, gmodel.metaBuilder.value, btp)

//                    val sett = getSetting(gmodel.settingFile.value)
//                    val kv = Kv().set(sett.getMap(""))
                                        val metas = Kv.by("rootRoute", "sutra")
                                                .set("rootRouteUpName", "Sutra")
                                                .set("rootRouteLowName", "sutra")
                                                .set("configName", "JxtproConfig")
                                                .set("projectName", gmodel.projectName.value)
                                                .set("projectFilterName", StrKit.firstCharToUpperCase(gmodel.projectName.value))
                                                .set("projectFilterLowName", StrKit.firstCharToLowerCase(gmodel.projectName.value))
                                                .set("projectPackage", gmodel.projectPackage.value)
                                                .set("baseSrcPath", gmodel.baseSrcPath.value)
                                                .set("baseResourcesPath", gmodel.baseResourcesPath.value)
                                                .set("baseWebAppPath", gmodel.baseWebAppPath.value)
                                                .set("baseWEBINFPath", gmodel.baseWEBINFPath.value)
                                                .set("db_dev_dbType", "mysql")
                                                .set("db_dev_jdbcUrl", "jdbc:mysql://localhost/aiopms?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useSSL=false")
                                                .set("db_dev_user", "root")
                                                .set("db_dev_password", "root")
                                                .set("db_dev_devMode", "true")
                                                .set("db_pro_dbType", "mysql")
                                                .set("db_pro_jdbcUrl", "jdbc:mysql://localhost/aiopms?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useSSL=false")
                                                .set("db_pro_user", "root")
                                                .set("db_pro_password", "root")
                                                .set("useShiro", "true")
                                                .set("useCron4j", "true")
                                                .set("usej2cache", "true")
                                                .set("useSelect", "true")
                                                .set("useZtree", "true")
                                                .set("useMode", "m2c")

                                        gen.build(metas)
//                    gen.build(kv)
                                        fire(GenEvent("generate success! "))
//                                        Conss.closeDb(gmodel.dataSource.value)
                                    } catch (e: Exception) {
                                        fire(GenEvent("generate failure: " + e.toString()))
//                                        Conss.closeDb(gmodel.dataSource.value)
                                    }
                                }
                            }
                        }
                        field("需要移除的表名前缀，用,分隔") {
                            textfield(gmodel.removedTableNamePrefixes) {
                                text = "t_, sys_,"
                            }
                        }
                    }
                    separator(Orientation.VERTICAL)
                    // 数据库中的表
                    fieldset(labelPosition = Orientation.VERTICAL) {
                        label(gmodel.tableCounts)
                        field("选择的数据库中的表有") {
                            listview(gmodel.tables) {
                                enableWhen(gmodel.dbOK)
                                selectionModel.selectionMode = SelectionMode.SINGLE
                                bindSelected(gmodel.leftTable)
                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (selectionModel.selectedItem.isNullOrEmpty()) {
                                        genSrv.prapare()
//                                    println(genSrv.getAllTables())
                                    })
                                }
                            }
                        }
                    }
                    fieldset() {
                        minWidth = 40.0
                        paddingTop = 100

                        button("->") {
                            prefHeight = 50.0
                            action {
                                run{
                                    //                            println(gmodel.leftTable.value)
                                    if (gmodel.excludedTable.contains(gmodel.leftTable.value) || gmodel.leftTable.value.isNullOrEmpty()) {
                                        return@action
                                    } else {
                                        gmodel.excludedTable.add(gmodel.leftTable.value)
                                        gmodel.tables.remove(gmodel.leftTable.value)

                                    }
                                }

                            }
                        }
                        button("<-") {
                            prefHeight = 50.0
                            action {
                                run{
                                    //                            println(gmodel.rightTable.value)
                                    if (gmodel.tables.contains(gmodel.rightTable.value) || gmodel.rightTable.value.isNullOrEmpty()) {
                                        return@action
                                    } else {
                                        gmodel.tables.add(gmodel.rightTable.value)
                                        gmodel.excludedTable.remove(gmodel.rightTable.value)
                                    }
                                }

                            }
                        }
                    }
                    fieldset(labelPosition = Orientation.VERTICAL) {
                        field("从左侧列表选择排除的表") {
                            listview(gmodel.excludedTable) {
                                id = "extble"
                                bindSelected(gmodel.rightTable)

                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (selectionModel.selectedItem.isNullOrEmpty()) {
//                                    println(genSrv.getAllTables())
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
