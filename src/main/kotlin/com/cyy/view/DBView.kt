package com.cyy.view

import com.cyy.model.GmodelModel
import com.cyy.view.generator.GenCenter
import com.cyy.view.generator.GenLeft
import tornadofx.*

class DBView() : View() {
    val gmodel: GmodelModel by inject()

    override val root = borderpane() {
        left {
            add(GenLeft())
        }
        center {
            add(GenCenter())
        }
    }
}



