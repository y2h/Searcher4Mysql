package com.cyy.controller

import com.cyy.view.MyFrag
import com.cyy.view.MyView
import tornadofx.*

class MyController : Controller() {
    val dataForView = stringProperty()
}


const val WINDOW_ID_VIEW = "view"
const val WINDOW_ID_FRAGMENT = "fragment"

const val PARAMETER_DATA = "data"

class OpenWindowEvent(val windowId : String, val data : String) : FXEvent()
class MyDispatcher : Controller() {
    init {
        subscribe<OpenWindowEvent> {
            if( it.windowId.equals(WINDOW_ID_VIEW) ) {
                find<MyView>().openWindow()
            } else if( it.windowId.equals(WINDOW_ID_FRAGMENT) ) {
                val params = mutableMapOf( PARAMETER_DATA to it.data )
                find<MyFrag>(params).openWindow()
            }
        }
    }
}
