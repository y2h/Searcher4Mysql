package com.cyy.srv

import com.cyy.model.Conss
import com.cyy.model.GmodelModel
import com.jfinal.kit.Kv
import com.jfinal.plugin.activerecord.generator.Generator
import com.jfinal.plugin.activerecord.generator.MetaBuilder
import java.sql.Connection
import tornadofx.*
import java.sql.DriverManager
import java.sql.ResultSet
import javax.sql.DataSource
import com.jfinal.plugin.activerecord.ActiveRecordPlugin
import com.jfinal.plugin.activerecord.Db
import com.jfinal.plugin.hikaricp.HikariCpPlugin


class GenSrv : Controller() {

    val gmodel: GmodelModel by inject()

    lateinit var con: Connection
    lateinit var rs: ResultSet
    var al = ArrayList<String>() // database list

    fun getCon(): Boolean {
        gmodel.jdbcUrl.value = "jdbc:${gmodel.dbtype.value}://${gmodel.host.value}:${gmodel.port.value}"
        Class.forName(gmodel.driver.value)
        try {
            con = DriverManager.getConnection(gmodel.jdbcUrl.value, gmodel.user.value, gmodel.pwd.value)

            return true
        } catch (e: Exception) {
            println("getCon failed ${e}")
            return false
        }
    }

    fun setJdbcUrl(dbname: String): String {
        return "jdbc:${gmodel.dbtype.value}://${gmodel.host.value}:${gmodel.port.value}/${dbname}?serverTimezone=GMT%2B8&characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false&zeroDateTimeBehavior=convertToNull&useInformationSchema=true&useSSL=false"
    }

    fun createDb(dbname: String): Boolean {
        Class.forName(gmodel.driver.value)
        con = DriverManager.getConnection(setJdbcUrl("information_schema"), gmodel.user.value, gmodel.pwd.value)
        if(con.prepareStatement("CREATE DATABASE ${dbname}").execute()){
            con = DriverManager.getConnection(setJdbcUrl(dbname), gmodel.user.value, gmodel.pwd.value)
            val sql = """CREATE TABLE `user` (`id` INT NOT NULL AUTO_INCREMENT,`name` VARCHAR(50) NOT NULL DEFAULT '0',`age` INT NOT NULL DEFAULT '0',PRIMARY KEY (`id`),UNIQUE KEY `name` (`name`));"""
            con.prepareStatement(sql).executeUpdate()
        }

        return true
    }

    // DROP TABLE  IF EXISTS `user`;
    fun createDb1(dbname: String): Boolean {
        val dbPlugin = HikariCpPlugin(setJdbcUrl("information_schema"), gmodel.user.value, gmodel.pwd.value)
        //指定编码为utf8mb4
        dbPlugin.setConnectionInitSql("set names utf8mb4")
        val arp = ActiveRecordPlugin(dbPlugin)
        // 启动管理库
        dbPlugin.start()
        arp.start()
        // 建库
        Db.update("CREATE DATABASE ${dbname}")

        // 启动业务库
        val dbPluginTest = HikariCpPlugin(setJdbcUrl(dbname), gmodel.user.value, gmodel.pwd.value)
        val arpTest = ActiveRecordPlugin(dbname, dbPluginTest)
        dbPluginTest.start()
        arpTest.start()
        // 建表
        val testDb = Db.use(dbname)
        val sql = """CREATE TABLE `user` (`id` INT NOT NULL AUTO_INCREMENT,`name` VARCHAR(50) NOT NULL DEFAULT '0',`age` INT NOT NULL DEFAULT '0',PRIMARY KEY (`id`),UNIQUE KEY `name` (`name`));"""

        testDb.update(sql)
        // 写入数据
//        testDb.update("INSERT INTO `a` (`xx`) VALUES ('xxxxx')")
        // 查询
//        val list = testDb.find("SELECT * FROM `a` LIMIT 100")
//        println(list.toString())

        arpTest.stop()
        dbPluginTest.stop()
        arp.stop()
        dbPlugin.stop()

        return true
    }

    //    获取数据源
    fun getDataSource(jdbcUrl: String): DataSource {
        val dbPlugin = HikariCpPlugin(jdbcUrl, gmodel.user.value, gmodel.pwd.value, gmodel.driver.value)
        gmodel.arp.value = ActiveRecordPlugin(dbPlugin)

//        gmodel.arp.value.addSqlTemplate(FileSourceFactory().getSource("${File("").canonicalPath}\\doc\\sql", "all.sql", "utf8"))
        // 从classpath中加载all.sql文件
        gmodel.arp.value.addSqlTemplate("/generator/all.sql")
        dbPlugin.start()
        gmodel.arp.value.start()
        return dbPlugin.getDataSource()
    }

    /**
     * get database list
     * @return ArrayList<String> 数据库名称列表
     */
    fun getDBs(): ArrayList<String> {
        //先清空，再添加获取的数据库列表，避免每次点击重复添加相同的数据库
        if (al.isNotEmpty()) {
            al.clear()
        }

        rs = con.metaData.catalogs
        while (rs.next()) {
            al.add(rs.getString("TABLE_CAT"))
        }
        if (gmodel.dbs.isNotEmpty()) {
            gmodel.dbs.removeAt(0)
        }
        gmodel.dbs.addAll(al)
        return al
    }

    fun gen() {
        // 创建生成器
//        val generator = Generator(ds, bmpkg, bmpkgpath, mpkg, modelOutputDir)
        val generator = Generator(gmodel.dataSource.value, gmodel.bmpkg.value, gmodel.bmpkgpath.value, gmodel.mpkg.value, gmodel.modelOutputDir.value)

        // 配置是否生成备注
        generator.setGenerateRemarks(gmodel.remark.value)
//        generator.setBaseModelTemplate("projects/IdeaProjects/javafx/generator-sqlite/tpl/model_template.jf")
//        generator.setModelTemplate("projects/IdeaProjects/javafx/generator-sqlite/tpl/model_template.jf")
//        generator.setModelTemplate(gmodel.modelTemplate.value)
        generator.setModelTemplate("jfinal/model_template.jf")

        // 设置数据库方言
        generator.setDialect(gmodel.dialect.value)

        // 设置是否生成链式 setter 方法
        generator.setGenerateChainSetter(true)


        // 添加不需要生成的表名
        generator.addExcludedTable("data_dictionary", "data_dictionary_value",
                "file_uploaded", "sys_function", "sys_log", "sys_org",
                "sys_role", "sys_role_function", "sys_user", "sys_user_role")

        // 设置是否在 Model 中生成 dao 对象
        generator.setGenerateDaoInModel(true)

        // 设置是否生成字典文件
        generator.setGenerateDataDictionary(true)

        // 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
        generator.setRemovedTableNamePrefixes("four100_")

        // 生成
        generator.generate()
    }

    /**
     * 设置MetaBuilder的removedTableNamePrefixes，excludedTable等参数
     * @param metaBuilder
     * @param removedTableNamePrefixes
     * @param excludedTable
     */
    fun prepareMetaBuilder():MetaBuilder{
        val a = arrayListOf<String>()
        val aa = arrayListOf<String>()
        val c = gmodel.removedTableNamePrefixes.value.split(",")
//                    a.add("ay_")
//                    a.add("Ay_")
        a.addAll(c)
        val removedTableNamePrefixes = a.toTypedArray()
        aa.addAll(gmodel.excludedTable)
        val excludedTable = aa.toTypedArray()
        return Conss.setMetaBuilder(gmodel.metaBuilder.value,removedTableNamePrefixes,excludedTable)
    }
    /**
     * get all tables in selected databases for mysql
     * @return ArrayList<String> 选择的数据库中的表名称列表
     */

    fun getList(con: Connection, sql: String): ArrayList<String> {
        val tblList = ArrayList<String>()
        rs = con.prepareStatement(sql).executeQuery()
        while (rs.next()) {
            if (rs.getString(1).isNotEmpty()) {
                tblList.add(rs.getString(1))
            }
        }
        return tblList
    }


    /**
     * 当数据库切换时，得到所选择数据库中的所有表，并将新的jDbPro对象加入到engine的SharedObjectMap中
     */
    fun prapare() {
        // 停止之前的activeRecordPlugin
        if ((gmodel.arp.value != null)) {
            gmodel.arp.value.stop()
        }

//        gmodel.jdbcUrl.value = "jdbc:${gmodel.dbtype.value}://${gmodel.host.value}:${gmodel.port.value}/${gmodel.dbname.value}?characterEncoding=utf8&useInformationSchema=true&useSSL=false"
        gmodel.jdbcUrl.value = setJdbcUrl(gmodel.dbname.value)
        gmodel.dataSource.value = getDataSource(gmodel.jdbcUrl.value)
        gmodel.metaBuilder.value = MetaBuilder(gmodel.dataSource.value)
        gmodel.metaBuilder.value.setDialect(gmodel.dialect.value)
        gmodel.getTablesSql.value = "select table_name from information_schema.TABLES where TABLE_SCHEMA='${gmodel.dbname.value}'"

        gmodel.tables.clear()

        gmodel.tables.addAll(getList(gmodel.dataSource.value.connection, gmodel.getTablesSql.value))
        gmodel.dbOK.value=true
        Conss.addSharedObject(gmodel.engine.value, Kv.by("Db", Db()))
//                .set("gmodel", gmodel))
    }
}