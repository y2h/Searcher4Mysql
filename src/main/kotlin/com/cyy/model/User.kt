package com.cyy.model

import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*
import java.time.LocalDate
import java.time.Period


//class JFXComboBoxTestApp : App(Main::class, MyStyles::class) {
//
//    class Main : View() {
//
//        val user = UserModel(User("John Doe", "johnd", "oe", "jdoe@fake.io"))
//        val states = listOf("PA", "NJ", "CA", "NV", "ND", "WA", "FL")
//
//        override val root = vbox {
//            addClass(MyStyles.box)
//            form {
//                fieldset("User Information") {
//                    field("Name") {
//                        jfxtextfield(user.name)
//                    }
//                    field("Login") {
//                        jfxtextfield(user.login)
//                    }
//                    field("Password") {
//                        jfxtextfield(user.password)
//                    }
//                    field("Email") {
//                        jfxtextfield(user.email)
//                    }
//                    field("State") {
//                        jfxcombobox(user.state, states) // use of JFXComboBox
//                    }
//                }
//                buttonbar {
//                    jfxbutton("Print User").action {
//                        user.commit()
//                        println(user.item)
//                    }
//                }
//            }
//        }
//    }
//
//    class MyStyles : JFXStylesheet() {
//
//        companion object {
//            val bar by cssclass()
//            val box by cssclass()
//            val customerBtn by cssclass()
//
//            val defaultColor = Color.web("#4059a9")
//        }
//
//        init {
//            jfxButton {
//                backgroundColor += defaultColor
//                textFill = Color.WHITE
//            }
//
//            bar {
//                alignment = Pos.CENTER_RIGHT
//                spacing = 10.px
//            }
//
//            box {
//                prefHeight = 600.px
//                prefWidth = 800.px
//            }
//
//            customerBtn {
//                backgroundColor += Color.TRANSPARENT
//                jfxRippler {
//                    jfxRipplerFill.value = Color.GRAY
//                }
//            }
//        }
//    }
//
//}


class Person(val id: Int, val name: String, val birthday: LocalDate) {
    val age: Int get() = Period.between(birthday, LocalDate.now()).years
}

data class Person1(val name: String, val department: String)

class User(name: String, login: String, password: String, email: String, comment: String = "", valid: Boolean = true) {

    val nameProperty = stringProperty(name)
    var name by nameProperty

    val loginProperty = stringProperty(login)
    var login by loginProperty

    val passwordProperty = stringProperty(password)
    var password by passwordProperty

    val emailProperty = stringProperty(email)
    var email by emailProperty

    val stateProperty = stringProperty("")
    var state by stateProperty

    val commentProperty = stringProperty(comment)
    var comment by commentProperty

    val dobProperty = objectProperty<LocalDate>(LocalDate.now())
    var dob by dobProperty

    val validProperty = booleanProperty(valid)
    var valid by validProperty

    override fun toString() = "User {name=$name; login=$login; password=$password; email=$email; state=$state}"
}

class UserModel(value: User? = null): ItemViewModel<User>(value) {
    val name = bind(User::nameProperty)
    val login = bind(User::loginProperty)
    val password = bind(User::passwordProperty)
    val email = bind(User::emailProperty)
    val state = bind(User::stateProperty)
    val comment = bind(User::commentProperty)
    val dob = bind(User::dobProperty)
}