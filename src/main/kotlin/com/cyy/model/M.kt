package com.cyy.model

import javafx.beans.property.IntegerProperty.integerProperty
import javafx.collections.ObservableList
import javafx.scene.shape.Circle
import tornadofx.*
import java.time.LocalDate

/**
 * 保存查询历史
 */
class SqlHistory(sqlKey: String, sqlValue: String, dbname: String, table: String, kv: String) {
    val sqlKeyProperty = stringProperty(sqlKey)
    var sqlKey: String? by sqlKeyProperty

    val sqlValueProperty = stringProperty(sqlValue)
    var sqlValue: String? by sqlValueProperty

    val dbnameProperty = stringProperty(dbname)
    var dbname: String? by dbnameProperty

    val tableProperty = stringProperty(table)
    var table: String? by tableProperty

    val kvProperty = stringProperty(kv)
    var kv: String? by kvProperty

    override fun toString() = "$sqlKey=$sqlValue"
}

class SqlViewModel : ItemViewModel<SqlHistory>() {
    val sqlKey = bind { item?.sqlKeyProperty }
    val sqlValue = bind { item?.sqlValueProperty }
    val dbname = bind { item?.dbnameProperty }
    val table = bind { item?.tableProperty }
    val kv = bind { item?.kvProperty }
    val sqlList = mutableListOf<SqlHistory>().asObservable()

    // 用于渲染webview组件的模板文件
    val wbtmpl = stringProperty()
}

class Database(val id: Int, val name: String, val tbls: ObservableList<Table>)

class Table(val id: Int, val name: String, val kv: String)


class Subscription(id : Int, productName : String, renewalDate : LocalDate) {
    val idProperty = intProperty(id)
    val productNameProperty = stringProperty(productName)
    val renewalDateProperty = objectProperty(renewalDate)
}

class MyCircle(private val x: Int, private val y: Int, private var radious: Double, private var step: Int) {

    val nextCircle: Circle
        get() {
            if (radious + step > 60 || radious + step < 10) {
                step *= -1
            }
            radious += step.toDouble()
            return Circle(x.toDouble(), y.toDouble(), radious)
        }

}

class Point(var x: Double=0.0,var y: Double=0.0) {
    fun getDestination(distance: Float, angle: Double, level: Int): Point {
        val p = Point()
        p.x = x + Math.cos(Math.toRadians(angle)) * level.toDouble() * distance.toDouble()
        p.y = y - Math.sin(Math.toRadians(angle)) * level.toDouble() * distance.toDouble()
        return p
    }
}

