package com.cyy.util

class TreeBranchBuilder(var key: List<String>, var value: Any) {
    fun build(): PropertyTree {
        return key.stream()
                .reduce(PropertyTree(),
                        { a, b -> PropertyTree(b, if (a.isEmpty()) value else a) },
                        { a, b -> throw IllegalStateException("Parallel processing is not supported") }
                )
    }

}