package com.cyy.util

object StrUtil {

    /**
     * 去掉下划线并将字符串转换成帕斯卡命名规范
     *
     * @param str
     * @return
     */
    fun unlineToPascal(str: String?): String? {
        if (str != null) {
            val result = StringBuilder()
            val temp = str.split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in temp.indices) {
                if ("" == temp[i] || temp[i].isEmpty()) {
                    continue
                }
                result.append(fristToUpCaseLaterToLoCase(temp[i]))
            }
            return result.toString()
        }

        return str
    }

    /**
     * 去掉下划线并将字符串转换成驼峰命名规范
     *
     * @param str
     * @return
     */
    fun unlineToCamel(str: String?): String? {
        if (str != null) {
            val result = StringBuilder()
            val temp = str.split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            var falg = false
            for (i in temp.indices) {
                if ("" == temp[i] || temp[i].isEmpty()) {
                    continue
                }
                if (falg == false) {
                    falg = true
                    result.append(temp[i].toLowerCase())
                } else {
                    result.append(fristToUpCaseLaterToLoCase(temp[i]))
                }
            }
            return result.toString()
        }

        return str
    }

    /**
     * 将字符串首字母大写其后小写
     *
     * @param str
     * @return
     */
    fun fristToUpCaseLaterToLoCase(str: String?): String? {
        var str = str
        if (str != null && str.length > 1) {
            str = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase()
        }
        return str
    }

    /**
     * 将字符串首字母小写其后大写
     *
     * @param str
     * @return
     */
    fun fristToLoCaseLaterToUpCase(str: String?): String? {
        var str = str
        if (str != null && str.length > 1) {
            str = str.substring(0, 1).toLowerCase() + str.substring(1).toUpperCase()

        }
        return str
    }

    /**
     * 将字符串首字母大写
     *
     * @param str
     * @return
     */
    fun fristToUpCase(str: String?): String? {
        var str = str
        if (str != null && str.length > 1) {
            str = str.substring(0, 1).toUpperCase() + str.substring(1)
        }
        return str
    }

    /**
     * 将字符串首字母小写
     *
     * @param str
     * @return
     */
    fun fristToLoCase(str: String?): String? {
        var str = str
        if (str != null && str.length > 1) {
            str = str.substring(0, 1).toLowerCase() + str.substring(1)
        }
        return str
    }

}