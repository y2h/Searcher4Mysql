package com.cyy.util

import javafx.scene.Node

object GetScreenUtil {
    //	public static double getScreenX(Control control) {
    //		return control.getScene().getWindow().getX() + control.getScene().getX() + control.localToScene(0, 0).getX();
    //	}
    //
    //	public static double getScreenY(Control control) {
    //		return control.getScene().getWindow().getY() + control.getScene().getY() + control.localToScene(0, 0).getY();
    //	}
    //
    //	public static double getWidth(Control control) {
    //		return control.getBoundsInParent().getWidth();
    //	}
    //
    //	public static double getHeight(Control control) {
    //		return control.getBoundsInParent().getHeight();
    //	}

    fun getScreenX(control: Node): Double {
        return control.scene.window.x + control.scene.x + control.localToScene(0.0, 0.0).x
    }

    fun getScreenY(control: Node): Double {
        return control.scene.window.y + control.scene.y + control.localToScene(0.0, 0.0).y
    }

    fun getWidth(control: Node): Double {
        return control.boundsInParent.width
    }

    fun getHeight(control: Node): Double {
        return control.boundsInParent.height
    }
}
