package com.cyy.util

import java.awt.Font
import javax.swing.ImageIcon
import javax.swing.Icon
import java.io.UnsupportedEncodingException
import java.util.HashMap
import java.awt.GraphicsEnvironment
import java.nio.charset.Charset
import java.util.SortedMap
import java.awt.Image
import java.awt.Toolkit


object GuiUtils {
    /**************************** 编码  */

    // 下面两个转码结果一样
    var CHARSET_ISO_8859_1 = "ISO-8859-1"
    var CHARSET_US_ASCII = "US-ASCII"

    var CHARSET_UTF_16BE = "UTF-16BE" // java转义\ u后面跟的编码, 即Java Unicode转义字符
    var CHARSET_UTF_16LE = "UTF-16LE"

    var CHARSET_UTF_8 = "UTF-8"

    // 下面两个转码结果一样
    var CHARSET_UTF_16 = "UTF-16"
    var CHARSET_Unicode = "Unicode"

    // GB2312 < GBK < GB18030
    var CHARSET_GB2312 = "GB2312"
    var CHARSET_GBK = "GBK"
    var CHARSET_GB18030 = "GB18030"

    var CHARSET_Big5 = "Big5"

    /**************************** 算法  */
    // 可解密/解码算法
    var CRYPTO_ASCII = "Ascii"
    var CRYPTO_HEX = "Hex"
    var CRYPTO_BASE32 = "Base32"
    var CRYPTO_BASE64 = "Base64"
    var CRYPTO_URL = "URL"
    // 不可解密算法
    var CRYPTO_MD5 = "MD5"
    var CRYPTO_SHA = "SHA"
    var CRYPTO_SHA256 = "SHA256"
    var CRYPTO_SHA384 = "SHA384"
    var CRYPTO_SHA512 = "SHA512"

    /**************************** 字体 ****************************/

    /**
     * 所有字体.
     */
    var availableFontsMap = availableFontsMap()

    /**
     * 中文字体集.
     */
    var fontStyles_cn: Array<String>? = null
    /**
     * 中文字体.
     */
    var fontStyle_cn: String? = null
    /**
     * 英文字体集.
     */
    var fontStyles: Array<String>? = null
    /**
     * 英文字体.
     */
    var fontStyle: String? = null
    /**
     * 支持Unicode的字体集.
     */
    var fontStyles_un: Array<String>? = null
    /**
     * 支持Unicode的字体.
     */
    var fontStyle_un: String? = null

    var font12_cn: Font= Font(fontStyle_cn, Font.PLAIN, 12)

    var font13: Font= Font(fontStyle, Font.PLAIN, 13)
    var font13_cn: Font = Font(fontStyle_cn, Font.PLAIN, 13)

    var font14_cn: Font = Font(fontStyle_cn, Font.PLAIN, 14)
    var font14_un: Font = Font(fontStyle_un, Font.PLAIN, 14)
    var font14b: Font = Font(fontStyle, Font.BOLD, 14)
    var font14b_cn: Font = Font(fontStyle_cn, Font.BOLD, 14)

    var font16: Font = Font(fontStyle, Font.PLAIN, 16)

    /**************************** 文件大小单位  */
    var FileSize_PB = "PB"
    var FileSize_TB = "TB"
    var FileSize_G = "G"
    var FileSize_M = "M"
    var FileSize_KB = "KB"
    var FileSize_Byte = "Byte"


    /**
     * 当前Java虚拟机支持的charset.
     */
    fun availableCharsets(): SortedMap<String, Charset> {
        return Charset.availableCharsets()
    }

    /**
     * 支持的字体.
     */
    fun availableFonts(): Array<Font> {
        val environment = GraphicsEnvironment.getLocalGraphicsEnvironment()
        return environment.allFonts
    }

    /**
     * 支持的字体.
     */
    fun availableFontsMap(): Map<String, Font> {
        val fonts = availableFonts()
        val fontsMap = HashMap<String, Font>()
        for (font in fonts) {
            fontsMap.put(font.fontName, font)
        }
        return fontsMap
    }

    /**
     * 字符串前缀字符填充.
     */
    fun addFillString(string: String, fill: String, interval: Int): String {
        val sb = StringBuilder()
        val len = string.length
        val loop = len / interval
        for (i in 0 until loop) {
            sb.append(fill).append(string.substring(interval * i, interval * (i + 1)))
        }
        if (loop * interval != len) {
            sb.append(fill).append(string.substring(loop * interval, len))
        }
        return sb.toString()
    }

    /**
     * 字符串前填充以满足固定长度.
     */
    fun fillStringBefore(string: String, fill: String, size: Int): String {
        val sb = StringBuilder()
        val len = string.length
        for (i in 0 until size - len) {
            sb.append(fill)
        }
        return sb.append(string).toString()
    }

    /**
     * 优先使用前面的字体，如果不存在，则一个一个向后查找..
     */
    fun getAvailableFont(fontStyles: Array<String>): String {
        var fontName = ""
        for (fontStyle in fontStyles) {
            if (availableFontsMap.containsKey(fontStyle)) {
                fontName = fontStyle
                break
            }
        }
        return fontName
    }

    /**
     * 右填充字符串.
     *
     * <pre>
     * 依据UTF-8编码中文占三个字节, 英文占一个字节, 且宋体下中文占两个半角空格位, 英文占一个半角空格位的特点;
     * 变通填充为字符中有一个中文字符即当两个半角空格位, 一个英文字符即当一个半角空格位.
    </pre> *
     */
    fun getFillUpString(string: String, size: Int): String {
        val sb = StringBuilder(string)
        var len = 0
        try {
            len = string.length
            len = len + (string.toByteArray(charset("UTF-8")).size - len) / 2
        } catch (e: UnsupportedEncodingException) {
        }

        for (i in 0 until size - len) {
            sb.append(" ")
        }
        return sb.toString()
    }

    /**
     * 计算文件(单位)大小，单位为字节Byte.
     */
    fun getCountFileSizeUnit(size: String, unit: String): Double? {
        return getCountFileSizeUnit(if (size.isEmpty()) null else java.lang.Double.parseDouble(size), unit)
    }

    /**
     * 计算文件(单位)大小，单位为字节Byte.
     */
    fun getCountFileSizeUnit(size: Double?, unit: String): Double? {
        if (size == null) {
            return null
        }
        var bSize: Double? = null
        val cas = 1024
        if (unit == FileSize_Byte) {
            bSize = size
        } else if (unit == FileSize_KB) {
            bSize = size * cas
        } else if (unit == FileSize_M) {
            bSize = size * cas.toDouble() * cas.toDouble()
        } else if (unit == FileSize_G) {
            bSize = size * cas.toDouble() * cas.toDouble() * cas.toDouble()
        } else if (unit == FileSize_TB) {
            bSize = size * cas.toDouble() * cas.toDouble() * cas.toDouble() * cas.toDouble()
        } else if (unit == FileSize_PB) {
            bSize = size * cas.toDouble() * cas.toDouble() * cas.toDouble() * cas.toDouble() * cas.toDouble()
        }
        return bSize
    }

    /**
     * 获取类加载路径下的图标.
     */
    fun getIconFromClassloader(classLoaderImagePath: String, kit: Toolkit): Icon {
        return ImageIcon(getImageFromClassloader(classLoaderImagePath, kit))
    }

    /**
     * 获取类加载路径下的图片.
     */
    fun getImageFromClassloader(classLoaderImagePath: String, kit: Toolkit): Image {
        val imgURL = ClassLoader.getSystemResource(classLoaderImagePath) // 这种方式可以从jar包中获取资源路径
        return kit.getImage(imgURL)
    }
}