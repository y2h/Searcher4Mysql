package com.cyy.util


import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Tooltip
import javafx.stage.Window
import javafx.util.Duration
import org.controlsfx.control.Notifications
import org.controlsfx.tools.Utils

import java.util.Timer
import java.util.TimerTask

object TooltipUtil {
    fun showToast(message: String) {
        showToast(null as Node?, message)
    }

    fun showToast(node: Node?, message: String) {
        val window = Utils.getWindow(node)
        var x = 0.0
        var y = 0.0
        if (node != null) {
            x = GetScreenUtil.getScreenX(node) + GetScreenUtil.getWidth(node) / 2
            y = GetScreenUtil.getScreenY(node) + GetScreenUtil.getHeight(node)
        } else {
            x = window.x + window.width / 2
            y = window.y + window.height
            //			Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
            //            x = screenBounds.getWidth() /2;
            //            y = screenBounds.getHeight();
        }
        showToast(window, message, 3000, x, y)
    }

    fun showToast(window: Window, message: String, time: Long, x: Double, y: Double) {
        val tooltip = Tooltip(message)
        tooltip.isAutoHide = true
        tooltip.opacity = 0.9
        tooltip.isWrapText = true
        tooltip.show(window, x, y)
        tooltip.anchorX = tooltip.anchorX - tooltip.width / 2
        tooltip.anchorY = tooltip.anchorY - tooltip.height
        if (time > 0) {
            Timer().schedule(object : TimerTask() {
                override fun run() {
                    Platform.runLater { tooltip.hide() }
                }
            }, time)
        }
    }

    fun showToast(message: String, pos: Pos) {
        showToast(null, message, null, 3.0, pos, null, null, true, true)
    }

    fun showToast(title: String, message: String, pos: Pos) {
        showToast(title, message, null, 3.0, pos, null, null, true, true)
    }

    @JvmOverloads
    fun showToast(title: String?, message: String, graphic: Node? = null, hideTime: Double = 3.0, pos: Pos = Pos.BOTTOM_CENTER,
                  onAction: EventHandler<ActionEvent>? = null, owner: Any? = null, isHideCloseButton: Boolean = true, isDarkStyle: Boolean = true) {
        val notificationBuilder = Notifications.create().title(title).text(message).graphic(graphic)
                .hideAfter(Duration.seconds(hideTime)).position(pos).onAction(onAction)
        if (owner != null) {
            notificationBuilder.owner(owner)
        }
        if (isHideCloseButton) {
            notificationBuilder.hideCloseButton()
        }
        if (isDarkStyle) {
            notificationBuilder.darkStyle()
        }
        Platform.runLater { notificationBuilder.show() }
    }
}
