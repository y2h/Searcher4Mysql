package com.cyy.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @ClassName: MyLogger
 * @Description: 日志记录工具
 * @author: Xufeng
 * @date: 2017年5月10日 下午9:22:38
 */
class MyLogger(c: Class<*>) {
    private val LOGGER: Logger

    init {
        this.LOGGER = LoggerFactory.getLogger(c)
    }

    fun error(message: Exception) {
        this.LOGGER.error(message.message)
    }

    fun error(message: String) {
        this.LOGGER.error(message)
    }

    fun error(message: String, throwable: Throwable) {
        this.LOGGER.error(message, throwable)
    }

    fun debug(message: String) {
        this.LOGGER.debug(message)
    }

    fun info(message: String) {
        this.LOGGER.info(message)
    }
}