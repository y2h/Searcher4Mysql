package com.cyy.util

object ValueConverter {
    fun asObject(string: String): Any {
        if (string.equals("true", ignoreCase = true) || string.equals("false", ignoreCase = true)) {
            return java.lang.Boolean.valueOf(string)
        } else {
            try {
                return Integer.parseInt(string)
            } catch (e: NumberFormatException) {
            }

            try {
                return java.lang.Long.parseLong(string)
            } catch (e: NumberFormatException) {
            }

            try {
                return java.lang.Double.parseDouble(string)
            } catch (e: NumberFormatException) {
            }

            return string
        }
    }
}