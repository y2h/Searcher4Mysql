package com.cyy.util

import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml

class YamlPrinter(val mainMap: PropertyTree) {
    operator fun invoke(): String {
        val yaml = Yaml(dumperOptions())
        return yaml.dump(mainMap)
    }

    private fun dumperOptions(): DumperOptions {
        val dumperOptions = DumperOptions()
        dumperOptions.defaultFlowStyle = DumperOptions.FlowStyle.BLOCK
        dumperOptions.indent = 4
        dumperOptions.defaultScalarStyle = DumperOptions.ScalarStyle.PLAIN
        dumperOptions.lineBreak = DumperOptions.LineBreak.UNIX
        dumperOptions.isPrettyFlow = true
        return dumperOptions
    }
}
