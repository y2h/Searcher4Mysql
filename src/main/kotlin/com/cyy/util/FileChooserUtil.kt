package com.cyy.util


import java.io.File
import java.nio.charset.Charset
import javax.swing.filechooser.FileSystemView
import org.apache.commons.io.FileUtils
import javafx.event.EventHandler
import javafx.scene.control.TextField
import javafx.scene.control.TextInputControl
import javafx.scene.input.DragEvent
import javafx.scene.input.TransferMode
import javafx.stage.DirectoryChooser
import javafx.stage.FileChooser
import javafx.stage.FileChooser.ExtensionFilter

/**
 * @author jeremie
 */

/**
 * @ClassName: FileChooserUtil
 * @Description: 文件选择工具
 * @author: xufeng
 * @date: 2017年7月25日 上午10:09:15
 */
object FileChooserUtil {

    private val myLogger = MyLogger(FileChooserUtil::class.java)

    /**
     * @Title: chooseFile
     * @Description: 选择文件
     */
    @JvmOverloads
    fun chooseFile(vararg extensionFilter: ExtensionFilter): File? {
        var file: File? = null
        try {
            val fileChooser = FileChooser()
            fileChooser.title = "请选择文件"
            fileChooser.initialDirectory = FileSystemView.getFileSystemView().homeDirectory
            if (extensionFilter != null) {
                fileChooser.extensionFilters.addAll(*extensionFilter)
                // fileChooser.getExtensionFilters().addAll(new
                // FileChooser.ExtensionFilter("文本文件 (*.*.txt)", "*.txt"),
                // new FileChooser.ExtensionFilter("二进制的对象文件 (*.*.dat)", "*.dat"));
            }
            file = fileChooser.showOpenDialog(null)
        } catch (e: NullPointerException) {
            e.printStackTrace()
            myLogger.error(e)
        }

        return file
    }

    /**
     * @Title: chooseSaveFile
     * @Description: 选择保存文件
     */
    fun chooseSaveFile(vararg extensionFilter: ExtensionFilter): File? {
        return chooseSaveFile(null, *extensionFilter)
    }

    /**
     * @Title: chooseSaveFile
     * @Description: 选择保存文件
     */
    @JvmOverloads
    fun chooseSaveFile(fileName: String?, vararg extensionFilter: ExtensionFilter): File? {
        var file: File? = null
        try {
            val fileChooser = FileChooser()
            fileChooser.initialDirectory = FileSystemView.getFileSystemView().homeDirectory
            if (fileName != null) {
                fileChooser.initialFileName = fileName
            }
            if (extensionFilter != null) {
                fileChooser.extensionFilters.addAll(*extensionFilter)
                // fileChooser.getExtensionFilters().addAll(new
                // FileChooser.ExtensionFilter("文本文件 (*.*.txt)", "*.txt"),
                // new FileChooser.ExtensionFilter("二进制的对象文件 (*.*.dat)", "*.dat"));
            }
            file = fileChooser.showSaveDialog(null)
        } catch (e: NullPointerException) {
            e.printStackTrace()
            myLogger.error(e)
        }

        return file
    }

    /**
     * @Title: chooseSaveImageFile
     * @Description: 选择保存图片文件
     */
    fun chooseSaveCommonImageFile(fileName: String): File? {
        val file = chooseSaveFile(fileName, FileChooser.ExtensionFilter("All Images", "*.*"),
                FileChooser.ExtensionFilter("JPG", "*.jpg"), FileChooser.ExtensionFilter("PNG", "*.png"),
                FileChooser.ExtensionFilter("gif", "*.gif"), FileChooser.ExtensionFilter("jpeg", "*.jpeg"),
                FileChooser.ExtensionFilter("bmp", "*.bmp"))
//        return chooseSaveFile(fileName, null)
        return file
    }

    /**
     * @Title: chooseSaveImageFile
     * @Description: 选择保存图片文件
     */
    fun chooseSaveImageFile(fileName: String): File? {
        return chooseSaveFile(fileName, ExtensionFilter("All Images", "*.*"),
                ExtensionFilter("JPG", "*.jpg"),
                ExtensionFilter("PNG", "*.png"),
                ExtensionFilter("gif", "*.gif"),
                ExtensionFilter("jpeg", "*.jpeg"),
                ExtensionFilter("bmp", "*.bmp"),
                ExtensionFilter("ICO", "*.ico"),
                ExtensionFilter("RGBE", "*.rgbe"))
    }

    /**
     * @return 所选择文件夹
     * @Title: chooseDirectory
     * @Description: 选择文件夹
     */
    fun chooseDirectory(): File? {
        var file: File? = null
        try {
            val directoryChooser = DirectoryChooser()
            file = directoryChooser.showDialog(null)
        } catch (e: NullPointerException) {
            e.printStackTrace()
            myLogger.error(e)
        }

        return file
    }

    /**
     * @Title: setOnDrag
     * @Description: 添加文件拖拽获取路径
     */
    fun setOnDrag(textField: TextField, fileType: FileType) {
        textField.onDragOver = EventHandler { event ->
            if (event.gestureSource !== textField) {
                event.acceptTransferModes(*TransferMode.ANY)
            }
        }
        textField.onDragDropped = EventHandler { event ->
            val dragboard = event.dragboard
            if (dragboard.hasFiles()) {
                try {
                    val file = dragboard.files[0]
                    if (file != null) {
                        if (fileType == FileType.FILE) {
                            if (file.isFile) {
                                textField.text = file.absolutePath
                            }
                        } else if (fileType == FileType.FOLDER) {
                            if (file.isDirectory) {
                                textField.text = file.absolutePath
                            }
                        }
                    }
                } catch (e: Exception) {
                    myLogger.error(e)
                }

            }
        }
    }

    /**
     * @Title: setOnDrag
     * @Description: 添加文件拖拽获取文件内容
     */
    fun setOnDragByOpenFile(textField: TextInputControl) {
        textField.onDragOver = EventHandler { event ->
            if (event.gestureSource !== textField) {
                event.acceptTransferModes(*TransferMode.ANY)
            }
        }
        textField.onDragDropped = EventHandler { event ->
            val dragboard = event.dragboard
            if (dragboard.hasFiles()) {
                try {
                    val file = dragboard.files[0]
                    if (file != null) {
                        if (file.isFile) {
                            textField.accessibleText = file.absolutePath
                            textField.text = FileUtils.readFileToString(file, Charset.defaultCharset())
                        }
                    }
                } catch (e: Exception) {
                    myLogger.error(e)
                }

            }
        }
    }

    enum class FileType {
        FILE, FOLDER
    }
}
/**
 * @Title: chooseFile
 * @Description: 选择文件
 */
/**
 * @Title: chooseSaveFile
 * @Description: 选择保存文件
 */