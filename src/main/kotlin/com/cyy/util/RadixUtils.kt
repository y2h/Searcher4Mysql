package com.cyy.util

/**
 * 进制转换类.
 *
 * <pre>
 * 特别说明：此处的字符皆为1个字节8位为1个转换单位，因此：
 * 1，1个字节转16进制为2位16进制，不足前面补零；
 * 2，1个字节转10进制为3位10进制，不足前面补零；
 * 3，1个字节转8进制为3位8进制，不足前面补零；
 * 这个类中的所有进制字符转换方法都是依据以上条件而定，当不满足以上条件时，程序会发生异常或不能得到预期结果！
</pre> *
 */
object RadixUtils {

    /**
     * 字符串前填充以满足固定长度.
     */
    fun fillStringBefore(string: String, fill: String, size: Int): String {
        val sb = StringBuilder()
        val len = string.length
        for (i in 0 until size - len) {
            sb.append(fill)
        }
        return sb.append(string).toString()
    }

    /**
     * 将16进制字符转为10进制字符.
     */
    fun convertRadixString16To10(radix16: String): String {
        val sb = StringBuilder()
        for (i in 0 until radix16.length / 2) {
            sb.append(fillStringBefore(Integer.toString(Integer.valueOf(radix16.substring(i * 2, (i + 1) * 2), 16)),
                    "0", 3))
        }
        return sb.toString()
    }

    /**
     * 将10进制字符转为16进制字符.
     */
    fun convertRadixString10To16(radix10: String): String {
        val sb = StringBuilder()
        for (i in 0 until radix10.length / 3) {
            sb.append(Integer.toHexString(Integer.valueOf(radix10.substring(i * 3, (i + 1) * 3))))
        }
        return sb.toString()
    }

    /**
     * 将16进制字符转为8进制字符.
     */
    fun convertRadixString16To8(radix16: String): String {
        val sb = StringBuilder()
        for (i in 0 until radix16.length / 2) {
            sb.append(fillStringBefore(
                    Integer.toOctalString(Integer.valueOf(radix16.substring(i * 2, (i + 1) * 2), 16)), "0", 3))
        }
        return sb.toString()
    }

    /**
     * 将8进制字符转为16进制字符.
     */
    fun convertRadixString8To16(radix8: String): String {
        val sb = StringBuilder()
        for (i in 0 until radix8.length / 3) {
            sb.append(Integer.toHexString(Integer.valueOf(radix8.substring(i * 3, (i + 1) * 3), 8)))
        }
        return sb.toString()
    }

    /**
     * 将16进制字符转为2进制字符.
     */
    fun convertRadixString16To2(radix16: String): String {
        val sb = StringBuilder()
        for (i in 0 until radix16.length) {
            sb.append(fillStringBefore(Integer.toBinaryString(Integer.valueOf(radix16.substring(i, i + 1), 16)), "0", 4))
        }
        return sb.toString()
    }

    /**
     * 将2进制字符转为16进制字符.
     */
    fun convertRadixString2To16(radix2: String): String {
        val sb = StringBuilder()
        for (i in 0 until radix2.length / 4) {
            sb.append(Integer.toHexString(Integer.valueOf(radix2.substring(i * 4, (i + 1) * 4), 2)))
        }
        return sb.toString()
    }

}